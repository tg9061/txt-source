「……還不行……我還不能死！」

大海的正中央，響起某道充滿執念的說話聲。


漂流第十天的女孩子──是在貝魯卡王國王都的平民區食堂裡，擔任招牌女服務生的愛夏（十五歲）。她的雙眼布滿血絲，雙手緊緊抱著船舷，像是用力夾緊的老虎鉗，看起來十分可怕。

愛上某位鍊成師的她，為了用真實之愛（愛夏的說法）守護尋求自己幫助的他（愛夏的說法），以及保護現在也在等待自己的他（愛夏的說法），不但橫越了大陸，甚至還搭船出海了。


而被這位處於失控超特快列車狀態的戀愛中的少女抓住弱點──更正，是欠她人情的兩名冒險者，也就是平頭赫達烈與掃把頭比比特利，現在也倒在甲板上。


為了找到安迪卡，三人意氣昂揚地……或許該說半自暴自棄地揚帆啟航。

不過他們是外行人，胡亂航行當然就會在海上遇難，他們也已經嘗到滿滿的後悔滋味了。然而，他們現在只想要嘗到食物和水的滋味。

「……赫達烈先生，比比特利先生，抱歉，都是我的錯。」

愛夏從船舷滑下，趴倒在地，少見地以沮喪的語氣道歉。有如死屍的兩人身子猛然一震。

「……愛夏小妹，如果能平安回到陸地……你要不要和我一起生活呢？」

比比特利如此述說。在極限的狀況下，人類似乎會本能地想留下後代，他可能就是處於那種情況吧？比比特利不顧愛夏與搭檔的困惑，有如吐露獨白似地開始說道：

「奧斯卡他已經去了遙遠的地方。所以，你就將就點，和我──」

「欸，我不要。」

愛夏非常嚴正地拒絕。直到剛才，她的聲音還虛弱得像快死掉的人，這句回答卻非常清晰明確。

比比特利遭到愛夏的精神攻擊，心都快碎滿地了，但因為這可能是人生最後的機會，所以他堅持努力不懈！

「我、我也能理解愛夏小妹的心情，但是近在身邊的幸福──」

「我不要。」

「如果是和愛夏小妹的話，我──」

「我真的不要。」

「有沒有一點可能性──」

「沒有。」

「……」

「糟糕了，赫達烈先生。比比特利先生的腦部似乎受創了，他看見不可能出現的幻覺了。不過才短短三天不吃不喝，竟然就變成這副德性……實在有點沒用呢。我靠著對奧斯卡先生的愛意還可以再戰十年的說！啊，奧斯卡先生！這種程度的試練，你的愛夏一定會成功跨越的！」

「……嗚嗚。」

「別哭，比比特利！現在浪費水分可是會致命的哦！」

比比特利淚如雨下，讓人不禁懷疑原來他身上的水分還那麼充足啊。儘管覺得他是自作自受，但是赫達烈因為搭檔面臨危機，於是大聲發出警告，其實也是在無謂地消耗體力。

然後，愛夏也沒資格說別人，她的腦部似乎同樣受創，開始看見她與奧斯卡相愛的幻覺，毫無意義地浪費了水分和體力。


三人都處於全力朝死神衝刺的狀況，最後終於無法動彈……

「喂，你們三個沒事吧？」

三人看見一艘不知何時靠了過來的巨大帆船，以及在船舷探出頭的貓人族女性。她擺動著貓耳，美麗的白髮在太陽下閃閃發亮。

「好、好美……」

真正的愛情似乎來到比比特利的面前了。

---

在那之後，過了一段時間──

愛夏等人在梅爾基涅號的甲板上，大口享用食物和飲水。

「喂喂，吃得那麼急會噎到哦。靜下來慢慢吃吧。」

梅爾基涅海盜團的卡媞，又倒了一些水給他們。愛夏捶了捶胸口，正要向她道謝的時候……比比特利插嘴說道：

「卡媞小妹，謝謝你。你不只救了我們，甚至還請我們吃這麼美味的食物！既是美人又會做菜，真是太犯規了啦！」

「什、什麼啊，不用那樣恭維我，我也會送你們回陸地啦。」

「不是恭維，我說這些完全是出自真心，我從未見過像你這樣嬌柔的女性。」

「不、不要胡言亂語，快點吃飯啦！」

卡媞紅著臉，抖動著貓耳，把臉別了過去。她的動作似乎正好符合比比特利的喜好，讓他的心臟激烈地悸動。

「哦哦，居然這麼快就在搭訕卡媞，看來很有精神嘛。」

梅爾基涅號的副船長•克里斯笑嘻嘻地走來。

「呃～這次承蒙你們相救，真的非常感謝。」

愛夏這次終於代表三人開口致謝。克里斯把木桶推倒，當成臨時的椅子使用，並且揮了揮手要他們不用在意。

「倒是你們在這種地方做什麼？……有事要去安迪卡嗎？」

克里斯微微眯起眼睛。因為這三人不管怎麼看都是航海的外行人，說可疑也確實是很可疑。

做為負責在船島周圍巡邏的成員來說，對他們三人心懷警戒也是理所當然的事。事實上，梅爾基涅海盜團的成員正以包圍他們的態勢潛伏在甲板上，隨時都可以壓制三人。

克里斯的笑容背後隱藏著銳利殺氣，愛夏則是……

「安迪卡！您知道安迪卡嗎！？」

她猛然睜大雙眼，強烈的氣勢連克里斯都差點真的要拔刀了。

「喔、喔？知道是知道啦。」

「請告訴我要怎麼去！我無論如何都非去那裡不可！」

「冷、冷靜一點！小妹妹，你真可怕！你的眼睛布滿血絲！呼吸粗重！別靠我那麼近啦！」

「快點快點！來，我們出發吧！現在立刻就出發！」

「你給我冷靜一點！為什麼你這麼想去那裡！？那裡可是無法無天的孤島喔？」

「一切都是為了愛！」

「愛！？愛是什麼！？」

「愛就是粉碎擋在前方的所有敵人（情敵），掌握未來的力量！」

「愛真是可怕啊！不對，我不是問這個！」

克里斯對她吐槽。赫達烈他們把愛夏拉開，安撫她冷靜下來。愛夏終於冷靜下來後，咳嗽一聲說道：

「因為我要找的人可能在安迪卡，他名叫奧斯卡……」

克里斯與卡媞交換眼神。那是他們非常熟知的名字，克里斯等人再度心懷警戒。

「……小妹妹，你和他是什麼關係？」

「我是他的妻子。」

「妻子！？」

赫達烈和比比特利立刻更正她的說法，同時簡單說明事情經過。另外，愛夏也不時插嘴發表她對奧斯卡的愛。

在他們說明的期間，海盜團的成員也聚集到甲板上，心想「奧斯卡還真受女人歡迎」。看到愛夏露出戀愛少女的表情、大大地談論奧斯卡，大家似乎都很有興趣。這時卡媞哼了一聲。

「哼，什麼嘛，那傢伙果然只是個小白臉。」

看到卡媞的反應，克里斯臉上笑嘻嘻的。

「怎麼啦，卡媞，你看起來心情不好呢？那也難怪，因為你對他很有意思，而且奧斯卡也很迷穿女僕裝的你嘛。」

「喂，我才沒有對那傢伙──」

「那傢伙？很迷女僕裝？請你說詳細一點。」

「咿～你什麼時候來到我背後的！？」

愛夏如影子般悄然來到卡媞背後，在她的耳邊輕聲細語，卡媞頓時跳了起來。

她下意識發動固有魔法『加速』，與愛夏拉開距離，可是……

「這女孩是怎麼回事！？為什麼能跟得上我的速度！？她的動作也很奇怪！」

發達的技術與魔法沒有差別，想著奧斯卡的時候，愛夏的腳步能夠到達魔法的領域！大概！

「在愛之前沒有不可能的事！話說你跟奧斯卡先生是什麼關係！？你穿女僕裝服侍他了嗎！？你打扮成貓耳女僕喵喵叫了嗎！？」

「什麼喵喵叫啦！？」

之後好一段時間，卡媞四處逃竄，愛夏則是以奇怪的動作緊追在後，兩人展開一場追逐戰，為海盜團炒熱了氣氛。另外，得知新的戀情前（可能）也有眼鏡男阻擋，比比特利哭倒在地，赫達烈則是眺望遠方的水平線。


之後，三人被帶到船島安迪卡，在那裡得知奧斯卡他們一連串的騷動……不過那又是後話了。
