「這下就、三十！！！」

在阿卡迪亞的通道上揮舞著長槍的格雷格，面對不斷涌來的帝国機鎧弄得氣喘吁吁。

「真是沒完沒了──呀」

在旁邊戰鬥的尤里烏斯，也對格雷格的觀點表示同意。

『是啊。但是，畢竟不能讓他們妨礙里昂呢』
「你是指芬恩的事嗎？雖然我倒是不想讓他跟里昂戰鬥啦」

格雷格知道里昂和芬恩是朋友。
正因如此，不想讓他們戰鬥。
不過，尤里烏斯有不同意見。

『那傢伙已經下定如斯決心。再阻止他就太不知趣了。』
「──也對啦」

待在二人身後的無人機，從背著的集裝箱裡取出替換的武器。
格雷格拋棄了破爛不堪的長槍，接過新的長槍。

「好，接下來是──嗯？」

馬上當場跳開後退，一架魔裝就從天花板落下。

「這些傢伙，太愛在自己的要塞上挖坑開洞了吧。」

身披魔裝的騎士，極度殘破。
來這裡之前就一直在勉強自己吧。
對方也手持長槍。

『你們這群敗北者的後裔。老老實實地滅亡就好了啊』

因為對方刺出了長槍，格雷格擋住攻擊縮短了距離。

「不肯放棄可是我的優點呀」

格雷格說著俏皮話，同時感受到對方的強大。

（這傢伙真強呀。這樣的傢伙竟然勉強跑到這裡來，證明他很焦急嗎？）

對方好像也了解到格雷格的強悍。

『太年輕（嫩）了。不只過度依賴機體性能，長槍的本領也是！』

正如對方所說。
比起格雷格，身披魔裝的騎士要更厲害。

（這傢伙、比我強。但是！）

「不好意思啊。就讓我依靠機體性能吧」

射擊藏在手臂中的機關槍，對手中彈後退。

『隱藏武器嗎！敷衍一時的蠢事！』
「只要能打敗你，我會不惜一切手段喔。我──我們是不能輸的啊！」

格雷格的長槍變紅。
刀刃發熱，那部分閃著紅光。
格雷格揮舞長槍，然後對方擋住攻擊的部分就融化了。

『咕！』

判斷抵擋攻擊太危險的敵人，先拉開了一次距離之後再接近。
對方把長槍突刺而出。

「這麼明顯的──」

但是，敵人以手持的長槍為誘餌──伸出魔裝那先端尖銳的尾巴部分，打算貫穿格雷格。
當尾巴尖端逼近駕駛艙時，格雷格馬上犧牲左臂來抵擋攻擊。

『精彩。但是，到此為止了！』

對身體動彈不得的格雷格，敵人打算用長槍將其刺穿。

「就這點程度，我哪會嚇得被幹掉啊！」

在駕駛艙內，格雷格扣動操縱杆的板機。
隨即，從裝在機鎧背包的集裝箱裡，發射出了好幾根樁子。
被那樁子貫穿，敵人離開了格雷格。

『可、可惡。要不是多虧那架機體的話⋯⋯』

對手悔恨地倒下了。
格雷格承認了這句話。

「是啊。你比我強喔。我能取勝，是多虧了機體性能」

對手笑了。

『承認得真乾脆呢。果然、年輕──』

格雷格將視線從不再動彈的魔裝騎士身上，移向朝自己襲來的敵人。
即使披著魔裝的騎士被打倒，帝国兵的士氣也並沒有如想像中的低落。

『你這失敗者！』

用長槍刺穿襲來的敵人機鎧，一腳踢飛後扔下了長槍。

「數量真多啊」

尤里烏斯把武器交給格雷格。

『對不起，支援晚了。』
「不用在意啦。沒讓其他人來打擾幫大忙了」

格雷格擔心的是前行一步的里昂。

（里昂，一定要回來喔。）


◇

讓路克西翁進行自動操縱的阿洛鋼次，到達了阿卡迪亞的動力爐。

動力爐在阿卡迪亞的中心部位，以柱子般的形狀存在著。
房間也與之配合，呈圓柱形。
黑色的柱子上，布滿了無數條如同血管般的紅線。
紅線就像在跳動一樣，微弱、然後強烈，重複閃爍著。

『這就是動力爐──產生魔素的裝置嗎？』

成功踏入了舊人類長年未能到達的地方，但路克西翁似乎有比那個更在意的事。

『MASTER，感覺怎麼樣？』
「糟透了」

立即回答的里昂，汗流浹背。
由於使用強化藥，對身體造成了相當大的負擔。
使用了中和劑，還有其他各種藥物，才總算能夠進行對話。

『畢竟直到剛才還神志不清，能作出思考就夠好了呢』
「嗯，多虧於此才正好趕上了重要場面。」

里昂一緊握住操縱杆，阿洛鋼次就打開集裝箱發射導彈。
當待在其身後的無人機們也同樣開始攻擊，動力爐就自動地展開防護罩來抵擋攻擊。
里昂皺起眉頭。

「就不能讓我簡單地結束嗎」
『建議靠近攻擊』

只要突破防護罩，就能毫無問題地摧毀它吧。
動力爐本身應該沒有建造得那麼結實。
即使真的很結實──阿洛鋼次的話也能夠摧毀它。

「我知道了。在這之前先補給吧」

因為武器用完了，里昂為了替換集裝箱而回頭。
阿洛鋼次分離集裝箱，進入接收的姿勢。
當正要接收其中一個由無人機背負著的集裝箱時，路克西翁就進行緊急回避。

『MASTER，是敵人！』
「在這個時機啊！」

無人機們被摧毀，然後爆炸。
雖然沒有全數被破壊，但是在有敵人的情況下要更換集裝箱是很困難的吧。
而且，對手太壊了。
路克西翁接收到了熟悉的聲音。

『唷──好久不見了呢』

圓柱狀的房間──從上面降下的是「布雷夫」

作為芬恩身披成機鎧的樣子，比其他魔裝大上一圈。

『布雷夫』

里昂露出了笑容。

「我好想你啊，芬恩！」

一邊這樣說著，里昂以全速後退，準備接收背包。
阿洛鋼次在機體構造上，產生推進力的噴射管等都安裝在背包上。
即使能漂浮起來，但沒有背包就無法加速。
不過，芬恩也預計到了里昂這個打算。

『我也是啊──里昂！』

當無人機們打算把背包交給阿洛鋼次，就被芬恩阻礙了。
芬恩放出火球，開始破壊無人機們。

「竟來妨礙！」
『我對你評價很高。所以，我不會留手的！」

面對著迫近而來的芬恩的布雷夫，路克西翁繼續計算著。

『MASTER，準備好了。』
「我的搭檔也很可靠呢」
『當然了。請不要把我跟布雷夫相提並論』

聽到兩人這樣的對話，布雷夫很生氣。

『真敢說啊！我會告訴你們，我和搭檔的組合是最強的！』

雖然里昂至今為止一直在後退，但當他停下腳步，無人機們就一同把集裝箱的開口對準了芬恩。

「躲開來看看吧，芬恩！」

以里昂的話做為信號，從集裝箱接二連三地發射出導彈。
無人機持有的武器也被盡數使用上，幾乎是要把自己帶來的彈藥全部打光的氣勢。
雖說很寬廣，但對機鎧之間的戰鬥來說是狹窄的通道。
芬恩馬上伸出左臂，變成盾形抵擋攻擊。
然後被炸飛了。
阿洛鋼次直接在地板上奔跑，追趕著芬恩。

『你認為這種程度就能解決我嗎，里昂！』

當芬恩回頭揮下劍，用戰斧接住的里昂就維持著那個體制。

「這樣就總會有辦法的！」

路克西翁注視著後方。

『來吧──施韋洛特』

從被毀壊的無人機的殘骸之中，作為背包之一的施韋洛特浮起飛來。
施韋洛特原本是一種叫做空中機車的跑在空中的摩托車，被路克西翁魔改造後現在變成了路克西翁的背包。
它像飛機一樣的身姿，當靠近路克西翁的背部就為了合體而放慢了速度。
當布雷夫想要攻擊它，阿洛鋼次就壓制住他。
里昂著急了。

「阿洛鋼次在力量上不是輸了麼」
『合體後輸出力就會增加，請耐心等待』

芬恩和布雷夫也很著急。

「那個背包是？麻煩了，黑助！」
『我正在幹！但是，這傢伙在礙事──該死！』

阿洛鋼次的後背與施韋洛特接合後，路克西翁馬上開始了行動。

『輸出上升。隨時都能行』

里昂推出操縱杆。

「啊啦！！！」

阿洛鋼次的雙眼發出紅色的光芒，將布雷夫撞開。
施韋洛特的裝甲滑動打開，激光從排列在上面的圓形透鏡中放出，襲向布雷夫。
芬恩遠離阿洛鋼次，把盾立在身前後退。
看到這一幕的里昂，向對方露出了後背，全速朝動力爐的方向前進。

『等、等一下！啊！這幫傢伙啊！！！』

殘留下來的無人機們，緊緊地抱住芬恩，進行阻礙。

「不好意思啊。我等會兒再跟你玩。」

雖然里昂在說著俏皮話，但卻是冒著冷汗。

『MASTER，就這樣優先破壊動力爐。現在戰鬥太危險了』
「我知道啦」

由於藥物的影響，並非正常狀態的里昂看起來很痛苦。
眯著眼睛。
一聽到後方的爆炸聲，路克西翁立即開始計算。

（看來會比想像的更快趕上我們）

雖然得到施韋洛特後推進力增強了，儘管如此，對方是魔裝。
布雷夫以前也曾是個固有稱號，總之肯定很優秀。

（只要優先破壊動力爐，MASTER就不會和芬恩戰鬥了。那樣的話，藥的使用也──就沒有必要）

路克西翁在意的，只有里昂是否使用藥物。
在考慮如何讓里昂平安地生還。
但是，路克西翁的願望也空虛地──布雷夫逼近過來了。

『里昂！！！！！！！』

路克西翁馬上修正布雷夫的能力數據。

（比預想要快。再這樣下去的話──）

看到芬恩追趕而來，里昂命令路克西翁。

「路克西翁──用藥」

被里昂命令的路克西翁，無法拒絶投藥。

（竟然這麼快就用第二次了）

『──了解，MASTER』

強化藥從里昂的背包裡注入。
然後路克西翁看到痛苦的里昂，就這麼想。

（我無力改變現況。）

里昂從痛苦中解放出來後，抬起了頭。
因為是第二次的投藥，血液立刻就從眼睛裡流出來。

（使用的間隔太短了。再這樣下去，第三次投藥的話，MASTER的身體會撐不住的）

里昂使阿洛鋼次轉過身，就這樣一邊向後飛一邊攻擊布雷夫。
激光襲向布雷夫，但被芬恩靈巧地不斷避開。
看了那個動作，路克西翁注意到了。

（難不成，對方也做了同樣的事嗎？）

布雷夫展現出了預料以上的性能，但如果說是芬恩象里昂一樣使用了藥物，那就說得通了。
接收到了芬恩和布雷夫的聲音。

『搭擋！不要胡來啊！」
『這時不勉強，什麼時候才勉強！為了米婭的未來，這點事兒算不了什麼！」

芬恩好像也使用著烈藥。
里昂似乎也聽到對話而察覺了情況。

「你也用了興奮劑嗎？我們真合得來呢，芬恩！」
『──連你也一樣哦！』

他們好像在互相取笑。
路克西翁，對讓這兩個人戰鬥而感到後悔。

（──要是沒有過去的事情，MASTER是否也就不用和友人戰鬥呢？）

那是，對自己成為了里昂的負擔的後悔。
穿過通道，阿洛鋼次再次來到動力爐所在的房間。
雖然馬上使用施韋洛特的激光進行攻擊，不過動力爐用魔法張開屏障阻擋了攻擊。

「就算是施韋洛特也不行嗎？」
『──是。但是，我判斷很難靠近攻擊。』

拔出大劍的布雷夫逼近了阿洛鋼次。
阿洛鋼次也從施韋洛特中拔出大劍接住了布雷夫的攻擊。

『不會讓你成功。米婭的未來──我不會讓任何人奪走！』

聽到芬恩的決心，里昂也吼道。

「我這邊也是關係到寶貝侄女的性命啊。豈能讓步啊！」

侄女──指前世的侄女艾莉卡。

但是，路克西翁知道。
里昂戰鬥的理由，並不只是為了艾莉卡。
如果只有艾莉卡，里昂只要讓她移居到安全的地方就完事了。
雖然嘴上說了很多，但這是為了今後出生在王国的孩子們。
如果是平時的里昂，肯定會不正經地說「那種正義感可不能相信吶」一類的話吧。
可是，雖然從里昂的態度上看不出來，但他比別人溫柔得多。

（──MASTER）

路克西翁所追求的是能替自己毀滅新人類的MASTER。
然後事到如今，里昂要替它實現了。
這讓路克西翁，非常難過。


════════════════════

若木醬（；゜∀゜）O彡°「⋯⋯」
若木醬（ノД｀）「對不起。有些事情我也是──做不到的啊。破壊這種氛圍，帶來歡笑是不可能的」