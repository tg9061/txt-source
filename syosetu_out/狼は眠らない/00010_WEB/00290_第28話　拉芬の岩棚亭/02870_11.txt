１１

之後，過了十二天都沒發生什麼事。

雷肯和阿利歐斯除了兩次修養日，依舊每天去迷宮，每兩天回來一次，也就是有了進迷宮就會在迷宮過一晚的習慣。
看到這情況，〈遊手好閒〉的三人似乎認為，〈虹石〉在逞強探索著。

這也是當然的。這迷宮的話，在階層途中退回階梯不會有任何危險，所以特地過夜探索的話，想必是在長期戰。也就是在不長期戰就贏不了的階層戰鬥。
納可也放棄去在意雷肯和阿利歐斯抵達哪一階層了。就算問了並回答了，大概也不會相信那答案。所以問了也沒用。

兩人是讓人愉快的客人，也不會鬧事或弄髒房間。會享受這宿屋的舒適感，也中意料理的美味。阿利斯歐會稱讚料理的滋味，雷肯雖然什麼都不說，但會好好品嚐，料理都會全部吃光。
而且有付住宿費，澡堂和酒的錢，每次也都會好好支付。

托〈遊手好閒〉和〈虹石〉的福，五間房間一直都是滿的。雖然高手冒険者中有不少會旁若無人，但〈遊手好閒〉和〈虹石〉都不會失去分寸。

〈遊手好閒〉有時會沒有預告就在外頭吃了晚飯才回來。交情也長了，這也沒辦法。在這場合就會在隔天早上吃。費用雖然是晚飯費用，但也不會抱怨，會好好付錢，所以納可也沒有不滿或損失。

作為宿屋的店主，實在是該滿足的狀況。
而且妻子的心情也好。
能說是沒什麼會掛在心頭上。

但是，在那天晚上，也就是一月二十六日的夜晚，聽到了〈遊手好閒〉和〈虹石〉的對話，讓納可心裡捲起了消不掉的暴風。
契機是，秦格的質問。

「話說回來，你們倆。在探索幾階層？」
「九十階層」

納可在櫃檯後方噴出了喝下的水。

「阿勒，真的嗎。這不是跟我們一樣嗎」
「真的呢。嚇了一跳阿」
「最高到達階層是哪裡？」
「今天才剛到達九十階層」
「咔咔咔。是嗎。九十階層的大型個體很難纏喔」
「是阿。我們有到九十二階層。你們追上來的話，要一起把九十三階層當目標嗎」
「哈哈，那還真不錯。秦格怎麼想？」
「嘩咔咔。不錯阿」

聽著這對話，納可在心理吶喊著。

（喂！）
（相信了嗎？）
（他們自從開始探索這迷宮算起來還不到二十天喔？）
（阿）
（〈遊手好閒〉他們不知道阿）
（是覺得來這宿屋前是住在別的地方）
（一直都有在探索）
（不是阿！）
（他們來了這宿屋後才開始迷宮探索的！）

「雷肯殿。怎麼樣呢？」
「呼嗯。既然難得這麼說了，到達九十二層的話就搭個話吧」
「喔喔！就那麼辦吧。咔咔咔」

（這就是目標嗎？）
（跟〈遊手好閒〉聯合探索就是他們的目的嗎？）
（是為此才住在這宿屋的嗎？）
（要是這樣，是打算對〈遊手好閒〉做什麼？）
（說到底沒有真的抵達九十二層就沒辦法聯合探索）
（也就是說現在真的到達九十階層了嗎）

迷宮探索要自行負責。兩支隊伍聯合探索的話，就算發生了什麼事故，那也是兩支隊伍的全員的責任。納可也沒有幹旋聯合探索，所以沒理由要擔心。儘管如此，一想到〈遊手好閒〉說不定會發生什麼不幸，納可的心就靜不下來。

（果然還是跟〈遊手好閒〉他們說一下吧）

納可在心中如此決定。

之後，晚飯的客人回去了，五位住宿客也進了房間。
到了隔天早上，〈虹石〉的兩人很早就出門了。

布魯斯卡在之後起床了。
把早飯拿到桌上時，納可開了口。

「〈虹石〉的那兩人阿，是在這個月的八日來這城鎮的」
「這個月的八日？啊啊，是從當時開始住這宿屋的阿」
「所以，是從這個月的九日開始探索茨波魯特迷宮的」
「嘿─。這樣阿」
「在說什麼東西。所以沒可能到達九十階層的」
「阿哈哈哈。沒可能吧」
「是吧」
「所以，是以前也有來過阿」

沒有那種感覺。
從那一晚的對話來看，雷肯和阿利歐斯毫無疑問沒潛入這迷宮過。但是要怎麼說明才好呢。

「那個叫雷肯的男人不是泛泛之輩喔」
「那也是吧」
「阿利歐斯那邊，也相當有本事」
「啊啊」
「有在早上見過，在庭院佇立的姿態嗎？」
「有」
「那很厲害。我能懂。那是很罕見的達人」
「雖然可能是那樣，但不論如何，十八天左右就從一階層抵達九十階層是不可能的吧」
「那種事當然沒可能阿。所以是以前也來過這城鎮好幾次」

被這麼一說，口才不好的納可，找不出還能說些什麼。

「在擔心什麼呢，納可先生。沒問題的。這邊有三人，那邊是兩人。一起探索看看，覺得合不來的話，只要馬上中止聯合探索就好。而且那兩人，不是壊人喔」
「我也不覺得他們是惡人」

總之該講的都講了。之後能做的只有守望而已。

不久後，〈遊手好閒〉也出門了。
這一天，〈虹石〉沒有回來，然後隔天傍晚，在挺早的時間回來了。