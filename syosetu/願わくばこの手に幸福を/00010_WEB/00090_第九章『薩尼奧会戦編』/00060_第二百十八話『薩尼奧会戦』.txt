天氣晴朗。風吹拂著身體，陽光有著與平時一樣的光輝。在那樣的好天氣裡，在薩尼奧平原，紋章教軍和大聖教軍，兩者互相對彼此露出了獠牙。

在戰場上粗暴的聲音，刀劍碰撞聲、悲鳴聲混雜在一起。誰都無法聽清聲音的具體來源。
內臟溢出、鮮血橫飛，士兵的生與死相互交織，創造了戰場這樣異常的場所。旁邊的戰友還活著嗎，還揮著劍嗎？啊啊，果然自己還活著嗎？很多人在廝殺時連這個都淡忘了。

劍、槍、盾，這些武器碰撞在一起，演奏著戰場特有的音樂。
誰都會睜開眼睛，貪婪地貪圖敵人的鬥爭本能。生命這種東西被安上最便宜的價格的地方，現在在這裡存在著。

無論是紋章教還是大聖教，都在平原內充分展開兵力。全部合計起來達到三萬的兵力互相碰撞，共同演奏起名為“戰爭”的奪命樂曲。

如果神在創造人類的時候埋入了鬥爭本能的話，那一定是為了聽這音樂。
“——旗色不好？那不是當然的嗎？雖然對此不高興，但是也沒辦法啊。”
被強迫作為先鋒的菲洛絲都市兵團中，菲洛絲·托雷特發出了牢騷。在單片眼鏡下面，被陽光照射著的白色的眼睛閃耀著。

敵兵和紋章教的士兵們在劣勢中，仍然為了爭取僅有的勝利之機，而揮舞著長槍。然後率領著他們的是魔女瑪蒂婭和惡德之主路易斯。聽說，他們擁有著用舌頭驅使他人，讓他人盲目相信自己的能力。

大概是由於被那樣的人所率領的緣故吧。敵兵們好像被什麼迷住了一樣，以如濁流般的氣勢向菲洛絲兵衝來。另一方面，菲洛絲兵團卻沒有同樣的氣勢和氣魄。只是按照命令架起盾牌挺起槍，堅守著不讓他們攻破陣型。

如果只是輪起士氣和氣勢的話，不用說菲洛絲的部隊，弄不好的話或許大聖教的部隊都要比起眼前的紋章教徒略遜一籌。
菲洛絲不由得咂嘴。正因為如此，菲洛絲才被強行安排為先鋒吧。這真是最忌諱的事。

士兵們狂暴的怒吼咬著菲洛絲的肌膚。身體的核心快要被人的聲音威脅似的的經驗，即使厲害如菲洛絲也是第一次感受的事。人的聲音，原來是這麼有力量的啊。
“菲洛絲大人，再稍微退下一點吧。如果只是呆呆地站在那裡的話，那還是退到大聖教的陣地比較好。否則會死掉的”
帶領著菲洛絲部隊的隊長用略帶口音的聲音說道，他在眉間的皺紋更加明顯，用惡毒的語言輕輕地刺穿菲洛絲。

和平時一樣，菲洛絲想，真是不知道客氣和擔心為而何物，堵不住嘴巴的人。
但是，這已經改不掉了吧。不管怎麼說，這個男人在被任命為都市兵的指揮官時也是這樣的態度。恐怕他這個人，是想到的事不直接說出口就會死的類型吧。

嘛，但是比起持續積存不滿要好得多。菲洛絲一邊把嘴唇朝上，一邊回答。
“那可不行。因為我是這個部隊的總指揮官。因為說著讓士兵們去死，而將他們送到了戰場。所以我不能在房間裡蹲著吧，也希望你能成為理想的指揮官。”

對於菲洛絲裝腔作勢的言語，隊長哼了一聲回應。對於菲洛絲這話怎麼也中意不起來。或許是因為隨心所欲的緣故吧，隊長能像他自己所想的那樣表現出全部的態度。這讓菲洛絲非常羨慕。

一個城市的統治者，比想像中更遠離自由。菲洛絲很想這麼告訴從前的自己。
作為統治者壓根沒有什麼好事。即使再怎麼追求，在那個座位上也不會有救贖之類的東西。
“現在還需要忍耐。敵人在集中攻擊中央。嘛，作為敵軍也只能瞄準那個了。”
隊長時不時發出怒吼，一邊指揮一邊嘟噥道。他的聲音沒有危機感和焦躁感，有很達觀的跡象，只想表達盡可能的去做的意思。雖然菲洛絲對戰場一無所知，但從這個隊長看來，戰場也許就是那樣的。

菲洛絲凝視著白色的眼睛，看著旗幟的搖曳。
確實，仔細一看，正如隊長所說的那樣，敵軍在中央部的旗幟經常搖晃。相反左右兩翼只是像忍耐一樣地固守著。雖然這麼說，只能說感到了隊長這麼說的心情。

也就是說，由於敵人數量較少，所以在中央部集中兵力，突破那裡的防御，從而將牙伸向大聖教的本陣。反觀己方，如果左右兩翼的部隊擊破了對方，那就能得到勝利。

本來，這項防守工作肯定是菲洛絲都市兵團——重裝步兵最擅長的。堅固地守護，防御，在防守中把握勝機。問題是，雖說這是自己擅長的工作，但菲洛絲有足夠在此阻止如此有氣勢的敵軍的兵力嗎？

這樣做也是沒辦法的事。最大限度的，讓城市不受影響，並且也能向大聖教那邊交待得過去的兵力，就是這個不滿千程度的兵數了。

第一，即使在這場戰鬥中獲勝，菲洛絲也什麼都得不到。要說希望獲得吞噬伽羅亞瑪利亞的權利，也不是那麼容易吧。這樣的戰鬥，能將城市的兵力大幅度分割出來嗎？本來就因為寒冷過度，食物就變得缺乏了。

菲洛絲濕潤了嘴唇，在野蠻的聲音飛來飛去的戰場中說道。
“盡量適可而止，不要讓士兵過度死亡。盡力向大聖教找藉口，然後退兵。”
像是要避免士氣動搖一樣，菲洛絲用除了隊長以外誰也聽不到的聲音說道。對於菲洛絲來說，必要的不是勝利，而是盡可能地減輕受害程度。而且，在我們身後還有兩萬名無傷的大聖教軍。即使自己早一點撤退，也不會對勝負產生影響吧。

即使是那位大聖教的老將，也一定考慮過這些事。既然這樣，就盡力讓自己做自己該做的事吧。
對於菲洛絲的這番話語，隊長背向她說道。
“……能做的話，就那樣做吧。”
這時，第一次讓隊長的聲音緊張的東西，誕生了。聲色本身並沒有改變。只是，那抑揚聲已經從聲音消失了。菲洛絲透過眼前的隊長，凝視著戰場。

馬奔馳在戰場。那簡直就像撕裂布匹一般的氣勢。
那個好像是一個部隊。敵軍中的一個分隊愚蠢地突出來，看起來好像飛出了自己的前線。

一般情況下，這樣的部隊一瞬間就會被敵軍圍攻而死。在戰場上孤立就是死亡。因為誰都知道這一點，所以就以軍隊的姿態整體向前邁進。
但是，那個小隊，不管經過多久，都沒有被幹掉。倒不如說他們正向這邊慢慢地接近。於是，其他的敵兵就像追隨這支小隊一樣開始向前進。
“菲洛絲大人，哎呀，他們到這裡來了。這些傢伙都是些胡來的傢伙。要作戰的話菲洛絲大人很礙事，別說得自己不會死一樣，趕緊掉頭逃走吧。”
隊長很郁悶，脖子上流著汗說。然後他慢慢地拔出了腰間攜帶的劍。
大概是出於動搖吧，儘管周圍的士兵發出粗暴的聲音拿著盾牌，但菲洛絲卻一步一步地向後退。

菲洛絲的白眼中，映射出那逐漸接近的威脅。
身穿綠色軍服，用馬蹄踩碎士兵的頭，向前行進。記得聽過，我記得我曾將那個姿態放入腦中。我記得曾經看到過描述他的那句話。

注入了惡意本身的雙眸，不畏懼神的違背道德的行為。
啊，沒錯吧。不會有錯的。那雙不動聲色就能將人踩死的眼睛，在戰場上尚未表現出一絲恐怖的神色，還有這種暴力行為。

菲洛絲的白眼，明確地抓住了那個人物。
——那就是，惡德之主。紋章教的英雄，路易斯。