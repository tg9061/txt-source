茨維特有兩位異母的兄弟姊妹。

一個是同年的弟弟，名叫庫洛伊薩斯，年紀十七歲。

因為生於同個時期，被身邊的人認為將會是繼承人之爭的競爭對手，但庫洛伊薩斯對別人完全不感興趣，總是以冷靜的態度隨便帶過。

茨維特從前在各方面就容易和人起衝突。老實說，庫洛伊薩斯並沒有把哥哥茨維特這個人看在眼裡。他對人類本身不感興趣，只專心傾注在魔法研究上。

在了解到他不是討厭人類，而是真心認為除了研究之外的事都無所謂的時候，茨維特就放棄對弟弟做出那種找麻煩的行為。

不過，這兩個人的態度對周遭帶來影響，徑自發展成了繼承人之爭，不過關於這件事，茨維特自己並沒有特別當作問題。

對沒興趣的事漠不關心，這點茨維特也是一樣。

另一人，則是在他旁邊專注分析、解讀魔法式作業的瑟雷絲緹娜。

這是他們的父親──德魯薩西斯，對當時在公爵家擔任女僕的女性「忍不住衝動」出手所生的側室之女。

他們異母兄弟倆的母親──第一、第二公爵夫人們知道這件事實，就立刻把瑟雷絲緹娜的母親從宅邸轟出去。

這在繼承人之爭的層面上是為防敵人增加的手段，同時也是想把吸引德魯薩西斯目光的她盡可能支開的計畫。

結果，瑟雷絲緹娜母親的身分就被祖父克雷斯頓給扛了下來，因為生出的是女孩，所以就不由得溺愛了孫女。

後來，瑟雷絲緹娜的母親年紀輕輕就因病過世，變得由克雷斯頓一個男人一手把她養大。然而，她那張遺傳母親的容貌，對公爵夫人們來說很礙眼，所以就把她視為了眼中釘。

夫人們的孩子──兩兄弟，特別是茨維特受到的影響深遠，從小時候就會做出陰暗的霸凌。庫洛伊薩斯則理所當然似的視若無睹，所以瑟雷絲緹娜就變得愈來愈閉門不出。

茨維特看來，索利斯提亞公爵家是這國家的王族分家血統，也是過去長達一百五十年以上保衛國家的魔導士一族。當然，他對祖先的偉業感到驕傲，同時夢想著有天也能保護國家或人民。

然而，他很不滿妹妹瑟雷絲緹娜不知為何使用不了魔法，卻還是待在自己敬愛的祖父身邊生活。他無法認可妹妹待在以「煉獄魔導士」馳名的祖父身邊，明明很無能，卻是自己的妹妹。重要的是，他受到母親討厭妹妹的影響，才會不抱任何疑問就對妹妹很冷淡。可是，瑟雷絲緹娜不是沒有才能，現在他明白那只是因為他以為理所當然的魔法式，其實有著滿是問題的缺陷。

就他聽見的傳聞，瑟雷絲緹娜要發動魔法本身是很困難的，課堂上的表現卻很優秀。作為魔導士是吊車尾，但因為其他部分全部獲得優秀的成績，因此她絕對不算是無能。然後現在，瑟雷絲緹娜的問題一切都解決了。

經由眼前的家教──大賢者之手……

「呃～所以，如果解讀這裡的魔法式，就會變成是『聚集魔力流動，必要魔量為10～50』。這東西表示在行使魔法上需要的魔力範圍，與可以控制的極限魔量。也有魔法式的極限數值──耐久魔量的這個含意。魔法式會需要預先決定好的魔力，但就算注入範圍之上的魔力，威力也不會提升。多餘的魔力反而會逆流，然後從魔法式中擴散出去……」

老實說，他本來很不喜歡這位大賢者。

可是，正因為自己尊敬的祖父認同他，重要的是他是個凌駕他人的實力者，所以他原本只是打算利用大賢者。雖然這只是到幾天前為止的事……

「換句話說，只要增減魔法式所需的魔量範圍，威力的強弱幅度也會改變嗎？透過簡單的魔法式，威力就會有所改變，對吧？」

「簡單來說是這樣，但可沒那麼單純喲。因為魔量變大，也會給儲存魔力並轉換成現象的魔法式耐久力帶來影響。

就算放入再多的魔力，如果構築魔法式的魔法陣很脆弱，魔力就只會擴散而已，這樣就會沒意義了。進一步地說，那也可能因為魔法陣崩壞，而引發失控現象，然後對周遭帶來巨大損害。不過，通常在那之前會先發生魔法式的崩壞現象就是了呢～」

大叔魔導士以好像很沒勁的言行繼續上著魔法式的課程。

老實說，茨維特沒想到會是這種境界的內容，所以覺得這個課程有趣到不行。倒不如說，因為杰羅斯淺顯易懂地說明了他不知道的事，他才得以順利地把知識裝入腦中。那些事非常新鮮，每一天都開始變得很有趣。

「真困難呢。也就是說，除了必要的魔力，還會需要提升魔法陣強度的術式嗎……」

「能讓那化為可能的就是積層型術式，透過兩種術式讓魔力循環，就是一種叫作『咒文回路』的東西。強大的魔法術式……我想想……例如，範圍型魔法，一般魔法陣不畫在大張的紙上就無法構築，對吧？透過把它分割成好幾份，將處理的魔法式插入其中，魔法陣就可以更緊湊地聚在一起了呢～」

出乎意料的優秀。

遇見對祖父之外感受到強烈憧憬的人物，還是他有生以來的頭一遭。畢竟杰羅斯不會討好當權者。不如說，他很看重自己的生活方式，甚至會毫不留情向敵對者挑起戰鬥，對於自己身為魔導士的生活方式也很自豪。

這或許是茨維特的價值觀發生變化所致的誇大，但從他眼裡看來，他認為杰羅斯是透過實戰與實踐進行驗證，不斷走在魔法頂端上的高階存在。而且，還是自己敬重的祖父認可的逸才。

他的祖父克雷斯頓，經常對執著權力的魔導士提出異議，給各派系帶來影響。雖然這被他過去隸屬的派系──惠斯勒派視作背叛，但實力差距太大，他們無法做出暗殺或警告這種舉止。比起這個，克雷斯頓有王族的血緣關係，無法貿然敵對也就是目前的狀況。

克雷斯頓一口咬定『那種執著權力的人不是魔導士，提升自我才是魔導士的原本樣貌』，而停止所有與派系的瓜葛。雖然規模很小，不過他創了獨自的派系。

眼前的杰羅斯，就像是具體表現那種魔導士理想的真實人物。

他是靠自己賺錢來研究，不許絲毫浪費，一路重複理論與實踐的賢者。

而且是實戰經驗豐富，連在戰場上的存活方式都熟知的瘋狂睿智探求者。

與杰羅斯相比，其他魔導士是多麼地不值一提，兩者有不容分說的懸殊實力差距。

「那麼，有沒有什麼其他問題？」

連魔法文字都能解讀的那種魔導士，如何能說他不優秀呢。

在他眼底，他認為這位凌駕他敬重的祖父的魔導士，是個遙不可及的存在。他這幾天受了指導之後，充分理解了自己是望塵莫及的小毛頭。

杰羅斯比伊斯特魯魔法學院的講師陣營優秀，即使如此卻不在乎派系，他這股傲慢的傻勁相當高尚，看起來比任何人都像位魔導士。

大部分魔導士會奉承貴族，或希望進入國家機關，這位魔導士卻是兩者皆非的例外。

「我了解魔法文字可以解讀。不過，個人學得到的魔法數量是不一樣的，對吧？要怎麼選擇適合自己的魔法才好咧？按照你的道理來說，所有屬性的魔法都不存在擅長與不擅長的隔閡，誰都能學習一切的魔法，但實際上我們使用的魔法，是會根據個人資質而有所區別的。目前也有出現一群屬性派系的傢伙呢。」

「一定是個人喜好問題吧。實際上，我就可以使用所有屬性的魔法，但我偏好使用復合魔法……而且，主要是在使用以雷系為基底的魔法呢。視狀況，我也會使用其他魔法。總之，雖然我有拚死學習偏好的魔法，但不喜歡的就容易變得馬馬虎虎的～這就會是個人的自由了呢。」

事實上，很多學生都會依賴擅長的系統魔法，而不學習其他魔法。

這是他多少能接受的答案。

「可以學，但無法運用自如嗎。不過，就算把魔法式刻在潛意識領域裡也有極限。如果要增加使用的魔法數，又該怎麼做才好呢？」

「那樣術式構築的範圍會太大。如果減少浪費、弄得緻密，收納在潛意識領域裡的魔法陣本身就會變小，匯聚多少就相對會剩下多少容許量，因此就可以學得下其他魔法數量了呢。總之，能理解魔法式到何種程度、能否順利使用，魔導士就各憑本事了。」

「他們現在正在研究的魔法，魔法陣是一個小競技場的面積大，確實很浪費呢。」

「競技場？你們是在研究殲滅魔法嗎～？不過，如果使用平面魔法陣，就是會變成那樣的吧。而且需要龐大魔力，我不認為可以使用……」

「說得好像都見過一樣……」

「因為如果是魔導士，那就會是必經之路呢。魔法愈強大，魔法式就會愈復雜化，所以我馬上就知道了。魔導士的本領可以藉由把那弄得多小來推斷呢。」

最新魔法研究就像是茨維特剛才說過的那樣，不過杰羅斯的魔法式是上下左右立體化這種不曾見過的術式。形式是透過魔法文字的循環組成令人費解的謎題。

至少，那比56音式的魔法陣還先進了一百年。對於可以學習這些知識，茨維特心中萌生出難以言喻，如優越感一般的情感。

「必經之路嗎……你到底領先了多遠啊。」

「誰知道呢？因為我無意把自己的研究和別人比較呢。我既沒興趣，也不打算告訴別人。」

「也就是說，你是叫我靠自己抵達嗎？抵達那種極端的境界……」

「當然會是那樣～我為什麼就得把抱著極痛苦的心情抵達的研究成果交給別人呢？畢竟也不知道繼承的人物會用在什麼地方。重要的是它太危險了。尤其是可以大範圍殲滅的那種魔法呢。」

「能夠教我們的完全就只是入門嗎？如果接下來創造出不妙的魔法，那你要怎麼辦？那也可以用56音式魔法製作，對吧？雖然據你所說，那會是密度相當高的魔法式……」

「我不會負到那種程度的責任。再說，就算知道我的研究成果，誰也沒辦法使用呢。這也沒有轉交給別人的意義吧。」

茨維特的背脊感受到一陣寒意。

這不是因為恐懼，不如說比較接近情緒高昂吧。

杰羅斯的話，總歸就是他做出的魔法完全會是自己專用，不是別人能夠使用的那種魔法。既然那是別人無法使用，只是單純存在的魔法的話，就不會有什麼意義了。

這位賢者已經身在頂點，茨維特對於能給這樣的人物教導魔法的真髓，感到無以名狀的喜悅。

「你們很年輕，首先只能從基礎靠自己的雙手精通。記住給別人教就高興得忘乎所以，這就等於停滯不前了。」

「也就是說，你要我自己創造出自己的魔法嗎？真是不得了啊，這可不是老師該說的話呢。」

「只要了解基礎並徹底應用，就會很簡單嘍。剩下的就是看能否持續努力。懷疑常識、與自己的戰鬥……理論與實踐，魔導士總是孤獨的呢……呵。」

「不，你為什麼要在那裡耍帥？」

這幾天，他了解到索利斯提亞魔法王國的魔導士有多麼落後。

現在認識了這位賢者，他痛切感受到國內的魔導士們誤會得多麼離譜。魔法文字的意義、解讀方式、魔法陣的構築、自己與自然界魔力的運用方式，以及操縱、應用──每一種都是他們不知曉的事。

甚至還考慮到實戰上的心理準備，或者近戰技術，他充分了解到壓倒性的本領差距。

「簡單……啊。也就是說，我也辦得到嗎？」

「辦得到喲。不過，之後就攸關個人的努力，以及對世界法則能理解多少呢。至少先了解物理法則會比較好吧。這是基本，先學起來會比較好。」

「物理法則啊，原來如此，這不是很有趣嗎……我還是第一次聽見這種令人興奮的事情耶。講師們到底有多沒用啊。」

「哥哥……那不是講師的錯，應該只是教導那位魔導士的人們弄錯的關係吧？誰都知道邪神戰爭後大部分關於魔法的文獻都遺失了，誰也沒想過自己正在研究的東西是錯誤的吧。我覺得也沒人擁有指謫的知識。一切應該都是維持在摸索狀態持續到今天的吧。」

茨維特雖然粗暴，關於魔法上卻很有熱情。

因此，他很認真的聽課，每次都會用自己的方式做起驗證。

眼前的魔導士抵達了頂點，就算這樣也不滿足地持續研究，面對有這樣的人存在，會想試著抵達那片領域，就是所謂魔導士的天性吧。

「那麼，時間差不多了呢。接著的課程明天再繼續吧。」

「咦，已經差不多了嗎？真快……」

「已經延長將近三小時嘍。要是不適時休息，塞太多也無法吸收。我覺得休息也很重要呢。」

「真遺憾。不過，我還真期待明天呢。」

「近期我會讓你們兩位挑戰積層魔法陣。那是很簡單的東西，就請你們輕鬆地玩玩吧。」

今天的課程就這樣結束了。

杰羅斯離開教室後，瑟雷絲緹娜也不忘複習。對妹妹這種模樣，茨維特藏不住心裡的驚訝。

因為如果是以前，她連接近自己都不會……

「欸，瑟雷絲緹娜……」

「什麼事，哥哥？……」

「你變了好多。如果是過去的你，你會最先從我身邊逃開呢。」

「是啊……不過，現在我也能使用魔法，雖然不到老師那種程度，但我也可以大略解讀魔法式。或許我已經和以前的自己不一樣了。雖然我沒有自覺……」

「那傢伙真是不得了的魔導士……我也很了解你為什麼會改變。」

雖然這只是兩人的主觀看法，但與杰羅斯相比，所有的魔導士都輸他一截。

能受到那種最頂尖魔導士的教導，對不成熟的魔導士來說是很光榮的一件事。

他們現在請杰羅斯教的課程，是魔法式的構築方法，屬於基礎知識。這部分也明顯與學院觀點不同，且是經過實證後的東西。

「和那傢伙相比，我根本就是個小嘍囉。我怎麼會像那樣找他打架呢？不過，請求賢者教導，對魔導士來說根本等於是最高榮耀。真好耶～你不是他的弟子嗎？」

「哥哥，你不也是弟子嗎？你不是正在上課嗎？」

「畢竟我有派系呢～是沒辦法成為正式徒弟的吧，學院畢業後我就是文職軍人了……啊～可惡！要是沒加入那種派系就好了！」

現在的惠斯勒派權力取向很強烈，魔法或研究戰略等等都是其次。

因為長時間待在那種地方，他在精神上也受到了污染。

他受到周圍讚揚，然後得意忘形了起來，那些結果就是嚴重的失戀。在某種意義上，就算說是被洗腦了也不為過。不，他開始懷疑自己實際上是不是受到了洗腦。

那是因為他回顧自己這兩年多的行為，實在有太多不自然的舉止。

想到是因為怎樣的理由而被解除，最近他腦袋莫名感到舒暢，就可以解釋那點。正因如此，他也有事情不得不先說。

「這是我的忠告。回學院之後，你要注意和派系有關的人們。他們會打算把你拉入陣營。」

「真稀奇耶。哥哥居然會給我忠告……這可是第一次喲。」

「我也不是笨蛋！現在的你改變得很多。你變得能使用魔法，也可以解讀魔法文字。變得甚至派系那些人不會對你置之不理……」

「真麻煩呢。我覺得派系根本就沒有任何價值……」

「完全同意。認識了大賢者這種怪物，就會覺得那些傢伙的課根本就是胡謅。從基礎開始根本完全就不對嘛！」

他們兩個這幾天來往密切，最重要的是很有充實感。

兩人的學習能力大幅躍升，彷佛至今停滯的東西一口氣傾瀉而出。

知道得愈多，愈是覺得魔法這東西很有趣，雖然說是基礎，但學習那些事情會引出新的發現。重要的是，曾經疑惑的事獲得解決，並且能看見嶄新可能性的這點很有趣。

「真不想回什麼學院～～～～！待在這裡，研究有進展多了。」

「是啊……再一個月就必須回去了呢……」

和杰羅斯的契約是兩個月。

所以再過一個月左右，他們就必須回學院上無聊的課程。

兩人認為那是浪費時間。

「話說回來，庫洛伊薩斯哥哥沒回來嗎？」

「庫洛伊薩斯～？那傢伙是聖杰魯曼派的重要候補，所以應該埋頭在研究裡吧。成天泡在研究大樓裡明明就是白費工夫……」

「是啊，那是白費工夫……他正在浪費寶貴的時間呢。」

「那傢伙說……『你要回老家嗎？那麼，請代我向父親問好。這點事情沒問題吧？畢竟你是我的親哥哥……』居然把別人當信差！」

「真是老樣子呢……他應該相當喜歡研究吧。」

次男庫洛伊薩斯只對研究感興趣，連和別人有瓜葛都認為是浪費時間。他對別人不感興趣，只把效率擺在優先，他那熱衷於研究的模樣，應該可以說是很有魔導士的樣子吧。

然而，對茨維特來說，他的言行也很令人厭煩。

「真是老樣子啊，那個裝模作樣的傢伙……但那傢伙運氣很差呢。呵呵呵……」

「啊……運氣確實很差。居然沒辦法上大賢者的課……」

「是吧？真期待看那傢伙表情的那一天……」

硬要比喻的話，他們就是火與冰。因為性格完全不同，他們知道彼此是絕對無法相容的存在。雖然這說不定只是他們沒有推心置腹地說過話……

「不過，那傢伙的態度很有魔導士的樣子……最近真是讓我有所思考了呢……」

「因為他的行為很像老師呢。但……他知識不足，而且還搞錯了。」

「這點很遺憾。雖然對我來說是很值得開心的事。是說，對耶……我當初會不喜歡那個傢伙，就是因為他很像庫洛伊薩斯嗎……」

「你就那麼討厭庫洛伊薩斯哥哥嗎？」

「最討厭了！那傢伙完全不想理我，周圍怎樣也都無所謂！我有天一定要灌那張裝模作樣的臉一拳！」

他不曉得因為個性不合而分歧，結果會變成公爵家繼承人之爭的理由。因為他們並不是敵對，只是因為不喜歡才沒有接近彼此。

他也是魔導士，別人的閑話根本無關緊要。

想到兩個哥哥這種狀況，瑟雷絲緹娜便嘆了口氣。

她只祈禱不要演變成內亂。

◇　◇　◇　◇　◇　◇　◇

隔日，杰羅斯來到客人用的接待室。

理由很單純，他是因為被溺愛孫女的瘋狂老人克雷斯頓給叫來的。

「呵呵呵……終於……緹娜的裝備終於完成啦！」

「您的興致異常高昂呢。我在這裡沒看見那個裝備耶。」

「老夫很高興，就忍不住交給緹娜了。她應該馬上就要來了……」

「您已經讓她試穿了啊……動作也太快……」

這位老人異常的興致高昂，等待穿著裝備的孫女到來。

克雷斯頓失控，杰羅斯就會忍不住吐槽。

「不能一直讓她穿著那種便宜裝備。畢竟，這是第一次為那孩子製作裝備呢。想必應該會很合適她吧。」

「……那另一位孫子呢？」

「他之前有新制衣服，所以應該沒差吧。而且男人的裝備都很粗糙不美觀呢。」

「……這爺爺還真是過分得乾脆。茨維特也真可怜……」

他太溺愛孫女而很偏心。茨維特得不到回報。

「這回老夫大筆揮霍了零用錢，從素材就精挑細選，準備了最棒的裝備。」

「真好奇是什麼素材，我很明白那恐怕花了大把銀子……」

「那當然！如果是為了可愛的孫女，老夫連靈魂都會出賣給惡魔！雖然是拿別人的命來賣啦……」

「這爺爺真是壞心得很乾脆呢。」

真是亂七八糟。而且，這老人是打從心裡這麼說的，所以非常惡質。

為了瑟雷絲緹娜，他恐怕會認真地把活祭品交給惡魔吧。

他對孫女的疼愛很罪孽深重。

「她早晚也會嫁人喔。到時您要怎麼辦？」

「……老夫不准許，老夫是不會准許的！我怎麼能把可愛的緹娜嫁給那些不三不四的傢伙！」

「要是那位可愛的孫女錯失婚期，您要怎麼辦？」

「屆時……對了，就嫁給你吧。放心，這國家的男魔導士是一夫多妻！反之也有一妻多夫呢。」

「請別若無其事地把我給卷進去！」

爺爺說出了不得了的話。

「這有什麼啊～……要是有了曾孫，不管你要消失到哪裡，我都無所謂喔。當然，即便是地獄也行……」

「把人當種馬再幹掉的幹勁十足啊。您有覺悟被我反過來殺掉嗎？」

「因為你要對老夫的可愛的孫女出手，那種程度的事就要給我做好覺悟。」

「您還真是墮落得很乾脆耶！老頭！」

在有關孫女的事情上，他完全就是個壞心爺爺。就被捲入的身分來說，這真的是教人受不了。不如說是太不講理了。

杰羅斯的吐槽也漸漸變得完全不客氣。

在他們爭論著蠢事的期間，旁邊的門就打了開來。裝備嶄新戰鬥道具的瑟雷絲緹娜從中現身。白色洋裝風格的服裝上，穿戴著銀白光澤的鋼制胸甲，手上拿著同色系單手用盾牌，連權杖都是絕品，雖然簡約卻裝飾得頗具品味。

連臂鎧跟靴子都加上不顯花俏的精細工藝，這番大張旗鼓的架勢讓人不禁覺得好像要上戰場一般。

「……那個～爺爺，那是要以魔物當對手進行實戰訓練，對吧？這、這是……」

「把秘銀纖維與艾爾克妮的絲混合織成的裝甲洋裝，外加秘銀與白蛇龍制成的麟甲。甚至有同系列的臂鎧和靴子……連權杖都是混黃銅，就算要當魔杖使用也是可行。老爺子……你到底花了多少？」

「你、你是指什麼？不是那麼大的數目……」

「這些裝備……不是寶物等級的嗎！況且，你還徹底按照自己的喜好走。」

「咦！咦──────────────！這是那麼昂貴的裝備嗎！」

克雷斯頓一臉佯裝不知，但可蒙混不過杰羅斯鑑定的眼力。

他顯然是使用了足以經營領地一年的預算。那實在不是光靠貴族零用錢就能設法做出的物品。

「克雷斯頓先生……我想再怎麼說也不會這樣，但您沒有私吞稅金吧？」

「真沒禮貌！那可是用老夫的錢去做的……雖然有賣掉一點寶物庫的貴金屬啦。」

「寶物？您有獲得現任領主殿下的許可嗎？總覺得那好像會需要相應的手續耶。」

克雷斯頓猛然把臉撇開。

換句話說，那是未經許可。既然是隱居身分，那就等同是侵佔。看來他好像為了孫女而著手犯罪。

「這樣不是很好嗎……德魯薩西斯那傢伙，說護衛不會叫來七個師團。既然這樣，就算為了女兒，準備這種程度的小錢也沒關係吧？」

「護衛又增加了耶！而且侵佔公款連反省都沒有！」

「我有好好只挑了老夫年輕時取得的貴金屬！我沒有沉淪到那種地步！」

「至少應該要辦手續。受牽連的可是您的孫女！」

「……唔！糟糕，我沒考慮到那裡……」

「您也太不謹慎了吧，您是不是快瘋了啊……」

這個老人的失控太離譜了。就算有怎樣的理由，既然是貴族的話，拿取與用途相符的金錢要辦正當手續就會是必要的。否則貴族也適用於侵佔罪。

這個老人一口氣跳過了那個過程。

「唔……這還真不妙。沒辦法……我去和德魯那傢伙道歉吧……」

「爺爺……這再怎麼說都太過火了。」

「在他說沒辦法的時間點，他就算是沒在反省了呢。雖然說自我中心很有魔導士的樣子，但就公爵家看來，這應該是最糟糕的吧。」

「真是……父親大人也真是讓人頭痛，我真是傻眼到不行，什麼話也說不出口了。」

「誰？」「唔……德魯薩西斯。」「父親大人……」

三人同時回頭，看見那裡站著一位和杰羅斯年紀相仿，儀表堂堂的中年紳士。

從剛才的話推測，杰羅斯判斷他就是瑟雷絲緹娜和茨維特的父親。

「我和大賢者閣下是初次見面呢，你的事我有從父親那裡聽說。我是瑟雷絲緹娜和茨維特的父親，也是那邊那位麻煩老人的兒子，是身為現任領主的德魯薩西斯。我們好像給你添了相當多麻煩。尤其……是那邊那位老頭……」

「您客氣了。我叫杰羅斯．梅林，只是個區區魔導士。請別叫我大賢者。是說，領主閣下，您好像很辛苦呢……」

「這個臭老爸未經允許就撬開寶物庫的鎖，把保管在裡面的幾個貴重魔石脫手賣掉，還謹慎地消除痕跡，甚至準備了不在場證明……我因為那樣，工作不曉得落後了多少……」

「爺爺……你是在幹嘛啊！」

克雷斯頓爺爺的汗水流得像條瀑布。

現任當家德魯薩西斯，眼神冰冷地盯著這樣的墮落老人。

「你、你怎麼知道……？我應該……沒留下證據才對……」

「你不在場證明的準備落空了。因為那種狀況不僅在時間上不可能，距離上也有點可疑之處呢。經過我仔細調查後，馬上就知道嘍。想不到你居然會準備替身……對方招供是被你用錢僱用的。」

「唔……那個酒店在距離上果然……而且那傢伙居然背叛了我！」

「你想說的就只有那些嗎？都怪你，負責警備的人還自殺未遂。我必須讓你扛起責任……」

「那些不三不四的人想變得怎麼樣，關我什麼事！」

他完全沒有反省之色。何止如此，態度還一副理所當然似的滿不在乎。

換個角度來看，那也可以說是將錯就錯。

「唉……算了，原因都是因為我玩火，所以我也沒辦法太責備你……可以的話，我還真想叫你起碼辦個手續。那麼一來，也就不會演變成這場騷動……」

「確實……仔細想想，這個爺爺會變這樣的間接原因就是您玩火呢。難道您不會偶爾也想停止玩火嗎？」

「不會耶。我的使命就是給心裡背負著悲傷的女人帶來喜悅！」

「居然這麼斷言……這就是所謂有其父必有其子嗎……父子真相似。」

「爺爺……您就算不做到那種地步也……」

從瑟雷絲緹娜看來，就會變成是因為自己的錯，才有人自殺未遂。

而且，原因還是祖父為了她而闖出的失控舉動。

「請看。瑟雷絲緹娜這不是受傷了嗎，就因為爸爸你的失控……」

「唔！……老夫也許確實有點過火，可是……寶物庫的警衛士兵，好像是因為結婚半年後，妻子和年輕男人外遇而煩惱……」

「那是醜聞的問題。自殺未遂的真正理由並不是這個問題。所以父親大人，我要請你好好善後。我是不會幫你做的喔，原因就是你失控。」

「沒辦法……雖然這不是我的本意，可是我就來善後吧……嘖……」

「這老頭咂嘴了耶……而且還很不情願……」

大叔的用字遣詞變得更糟了，已經完全稱他為老頭。

好像已經不打算對這個老人客氣。

「對了，德魯薩西斯先生，這個爺爺到底賣了什麼啊？」

「兩顆飛龍的魔石。那是手掌大小的寶貴魔石，如今要得到那個東西，應該已經是近似不可能的吧。那可是皇家賞賜的重要物品……」

「飛龍的程度嗎……？我有喔，有飛龍的魔石……」

「什麼！還請你務必給我。再怎麼說，我也不想看見親父親被處刑。」

「是可以……老爺子，你欠我一份人情喔。」

「唔嗯～……沒辦法了呢。那就麻煩你吧……」

他要是有走前面的步驟，就不會變成這種情況，但即使如此，這名老人的態度依舊頑固。要是扯上孫女的話，好像就會變得很好戰。

杰羅斯嘆了口氣，但還是操縱道具欄，拿出大約三顆飛龍魔石，接著遞給了德魯薩西斯。可是，在場三人的表情都瞬間僵住了。

「什麼！這是多麼大的……這有我們保管的一倍大耶！」

「唔……不愧是法芙蘭大深綠地帶，居然有這種程度的魔石……」

「我、我有聽說老師打倒了飛龍，但居然是這麼大顆魔石的飛龍……」

「這本來就免費。請不用客氣。我還有四顆左右，因為我也受到你們的照顧呢。」

三人啞口無言。

換句話說，那意味著他正面挑戰了七頭飛龍，而且還打敗了它們。

那原本就是成群狩獵的魔物，就可能性來說不會只有一隻，大部分傭兵甚至會在團體戰上出現眾多犧牲者。

那同時意味著眼前的魔導士一個人就與那麼多的戰力相抗衡。

「……真是感激不盡啊，大賢者。請你務必讓我彌補，我這笨蛋老爸也是……」

「我不需要地位和名聲，請給我一片土地。可以種田那種細致土地就行了。」

「嗯……話說回來，還有土地的這回事呢。最近紛擾不斷，我都給忘了……」

「你還真冒失呢，德魯薩西斯……」

「你還以為是誰的錯啊，你這個臭老頭……」

原因就是這位爺爺。

「雖然還沒安排，但這是你救了我這個臭老爸和女兒的回禮。我就開墾這座別館的一個角落，當作你的土地送給你吧。我也會順便送你一間房子。」

「謝謝。我總算……總算能脫離無家可歸了嗎……真是漫長……」

「那棟『慘叫教會』的後方也很方便吧？周圍是我們的領地，我想應該可以安靜地生活。那樣可以嗎？」

「可以得到房子的話，我不會在意小細節。畢竟沒工作又沒房子會很不體面……是說，慘叫教會！那是什麼鬼啊？」

「你不曉得啊？最近蔚為話題喔。人家都說是慘叫四起的噩夢教會呢。關於你的住家，我回到宅邸後就立刻安排吧。」

他沒想到孤兒院會被以奇怪的別名稱呼。

而且謠言還傳到了領主耳裡。看來栽種曼德拉草，社會觀感好像非常不好。

那就先不說，德魯薩西斯意外地是個能溝通的領主。要是他不拈花惹草就會很完美，但他本人完全無意停止玩火。

他掉過頭，就立刻準備要走出房間。

「那麼父親大人，我還有工作，就先回去了。還請您千萬別闖禍喔。」

「好啦！這次老夫有點太過火了……嘖……（下次得做得不露馬腳……）」

「「這老頭……完全沒學到教訓吧！」」

這是他與領主之間的短暫邂逅。

無論如何，杰羅斯總算是得到了自己期盼的土地和房子。

雖然是不久後的事，不過那棟房子將成為今後的活動據點。

「話說回來，我沒看見茨維特那傢伙呢。那傢伙在做什麼？」

「咦？您這麼一說，我今天確實沒看見他耶～」

「哥哥大概是在預習吧？」

「老夫沒發現呢。因為很無關緊要。」

真可怜，茨維特被徹底給遺忘了。

說到那樣的他正在做什麼……

「哈哈哈♪我了解了！原來如此，這就是魔法式的解讀啊！好久沒這麼開心了呢♪」

……他興高采烈地實踐魔法式的解讀。

不論平時態度如何，他都是既優秀且認真的魔導士。

這天算起的三天後，他們便前往了法芙蘭大深綠地帶。

在魔物徘徊的大森林裡開始了以實戰訓練為名的新兵訓練營。