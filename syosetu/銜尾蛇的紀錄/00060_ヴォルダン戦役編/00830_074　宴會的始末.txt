那個惡夢般的夜晚已經過了五天。

尤爾根・鮑爾在佔領都市的廳舍、這個執務室沉思著。這次沃爾丹州的侵攻以來發生著許多怪異的光景、想到了幾個有趣的相異部分。

首先第一、傷痕累累的他的身體。

克萊維爾之戰裡、聖加侖軍後背出現的吸血鬼。被怪物軍勢攻擊身負重傷的鮑爾、居然奇跡般的保住一命。本來、在那大混亂中。本來會使用治癒魔法的從軍神官、都作為不死族的天敵被優先殺死了。因此、治療他的不是神官而是魔導師。跟有著治癒秘蹟的神官不同、強行治療的身體表面居然沒有傷口。實際上胸口的傷還沒有痊癒、因為傷跡述說著疼痛。這種治療方式、是只有學院的知名教授才能做的手術。

接下來、明明是不思議的負傷、鮑爾的血色卻不壊。看來是因為軍隊的營養狀態改善了。得到了補給、並不是新的略奪成功了。而是軍隊讓一半以下的人減少食物、然後讓另一部分以人頭的方式增加食物。這個遠征軍長時間煩惱的問題、大敗的原因就是這麼諷刺。

然後最後的是⋯⋯他的表情、如同戴上了面具般的苦悶而固定著。

「該怎麼辦才好⋯⋯」

嘴唇裡說的就像是別的生物一樣──奄奄一息的──終於動了、吐出苦衷。

現在軍的總兵力為一萬六千。比起阿爾凱爾王國的沃爾丹殘兵是一倍以上。在外行人看來這會來露出樂観的想法吧。
沒什麼、那邊也削減了、對方的消耗也是一樣的。已經知道了對方的戰術經驗、只要制定對策在打一次就可以了。這次會贏吧⋯⋯這樣想。

但是、那是不行的。

以軍隊的兵數來說、這是不可能的事情。兵隊是無數的個人組合起來、一個一個組成軍隊的齒輪。四萬人只剩下一萬六千人的聖加侖軍、已經減少了二萬四千人、這已經相當於壊掉的機械。已經沒有能夠滿足條件讓他們動員起來了。

那麼就把軍隊再編讓一萬六千人動員起來、估計會怎麼想吧。但是、再編的作業不是說的那麼簡單。

對出現戰死者與重傷者的部隊、當然要進行新兵的補充。這就跟職場上新人融入新環境一樣、不是那麼簡單的事情。同時用新規則滿足新編隊的兵隊動員他們、也是必要的。前任者──死人與半死人──繼承那個職位的、原隊員與新入的人進行溝通、實際的訓練動作與確認連攜提高工作效率、等等。要做的事情要多少有多少、忽略哪一個都不行。

重整一度崩壊的軍隊、需要莫大的煩瑣作業。

⋯⋯面對敗北士氣低落的將兵、這個更加的艱難。

當然，這些作業正在快速運行中。如果不是這樣的話、那麼別說要與阿爾凱爾王國戰鬥、翻山越嶺逃回到本國也不是不可以。

但是、僅僅只是解決了這一週的問題之後。因為這是令大陸驚異的數字、因為只要不去看士氣與這支軍隊的打扮。那麼估計誰都會認為這支軍隊是不可戰勝的。

這也是使討伐數量較少的沃爾丹敵軍成為可能。對於再編來說阿爾凱爾側也是同樣的。如果是這樣的話、原熟練度較高的聖加侖方面比較擅長。然後捲土重來、壓制沃爾丹西部可能性並不是零。

但是、這要是面對後來支援的阿爾凱爾王國本隊則是沒有勝算。許容受到的損害已經遠遠超越了現狀、不僅是食物、人數實在是太少了。而且沒有時間。克萊維爾的戰鬥使西部侵攻受挫、陷入了敗戰必須重整軍隊的困境。行程安排被大幅度的延後。搞不好、再次侵攻之際敵人援軍就來了。即使援兵來得較晚、現在士兵也無暇休整。冬天一到來面對籠城戰的話、寡兵加上疲弊那麼勝利就更加茫然了。

總之沒有勝算。聖加侖聯邦王國的戰略方針，在開戰後不到一個月就完全崩潰了。

「該怎麼辦才好⋯⋯」

嘟噥完後又再次嘟噥。

到底該怎麼辦呢、事已至此絶對是要做出點什麼來的。快速再編完成、翻越山脈撤兵是最好的。因為打不贏、只能逃了。會再次選擇戰鬥下去損兵折將的只有那些無名的將帥而已。而且這全憑駐紮與此處鮑爾的個人意志。本人也十分明白。

但是、這裡有個問題。

在這裡吃了個大敗戰就這樣撤退的話人們會怎麼說？連區區沃爾丹一州都無法攻陷、還白白損失了二萬四千名士兵、毫無羞恥地逃回來了。這樣子、尤爾根・鮑爾與拜哈里耶王國就會成為連邦的笑柄。自己還好。但是、作為故郷的大王家就會被其他領邦彈劾、作為平民出身而沒有後盾的鮑爾的家族將會遭遇什麼事情是無法想像的。

為了避免這個結局、即使是勉強也必須打出戰果來。一次就好。把自己和全軍將士的性命都豁出去也沒有關係。在戰爭全體敗北的結局前、至少在面對戰果時自己必須有有面子。要人人們認為不愧是拜哈里耶、聖加侖領邦的精華啊。一場讓全土都稱賛的戰鬥。

不過、建立這種武功的可能性非常渺茫。本來有著充足的軍隊就已經很困難了。現在還是這種殘兵敗將、這種至難的挑戰基本不可能完成。這種天真的想法、就連他都以為是做夢。

果然現在還不能逃跑⋯⋯不對不對這種無謀的作戰怎麼可能成功⋯⋯⋯

思考著就這樣惡性循環、得不出答案。所以、該怎麼辦才好、不知不覺間都發出了呻吟。但是、就算在煩惱下去也沒有用。只能讓士兵再編結束理科行動了。
進退兩難。在保留與停滯的時間內必須做出選擇。在這樣空費時間、敵本隊的援軍就要趕來了。

在這二選一面前、鮑爾繼續苦悩著。
但是、那個時間也在今日結束了。

「出、出大事了！鮑爾將軍！」

一位部下、變了臉色的衝入房間。

鮑爾順便著消去了眉間的皺紋、但是表情更加嚴峻了。作為軍隊擔任中樞的總司令官、覺得房間裡發生這種騷亂可不是什麼好事情。

「別慌⋯⋯衛兵說了什麼──」
「不是說那個的時候！阿、阿、阿爾凱爾軍⋯⋯阿爾凱爾軍！」

完全失魂落魄的說不出話來、鮑爾也不想去叱責報告的人。但是、從眉間開始臉色變得青褪了、內容不是什麼喜訊已經確定了。

「──阿爾凱爾軍嗎、怎麼了？」

進行反問、給了部下呼吸二、三回的時間、總算是知道了他要表達的意味
只是、因為太晚自己無法否認而已。

「阿爾凱爾軍的襲擊！向這個城市、打過來了！」
「什、麼⋯⋯？」

難以置信。現在州內存在的阿爾凱爾王國的兵力、頂多只有六千人。這裡的人數超過了對方一倍以上、而且據點還是籠城。這個城市雖小但也是城市、在有著如此的兵力差的情況下。為什麼還會打過來？

相對的、阿爾凱爾側的士兵的再編也是必要的。一萬几的兵將近三分之二死去了。更重要的是敵兵的主力是農民。那些沒有訓練的新兵猜剛剛經歷了過酷的會戰、不可能在沃爾丹東部進行行軍攻城的。

那麼、是攻過來的敵軍是、

「來援的本隊、嗎？不對、但是⋯⋯」

這不可能。王都的兵集結、編成、然後在向沃爾丹州進發、這樣算起來自少也要下週才能到達。無論如何、現在通過主要街道外的東沃爾丹、到達是不可能的。

而且、要是援軍真的接近了、也沒有不會在敵人領主出站的克萊維爾盆地出現。即使特意不參加、也要在州都進行籠城戰時與本隊連攜、對聖加侖軍來個內外夾擊才對。這是不變的道理。

如果不是這個可能。那麼、現在出兵的究竟是？

「⋯⋯無暇考慮了。進行迎擊指揮──」
「不行！敵軍已經侵入到城市內部了！我方受到奇襲、指揮系統被破壊了！」

絶望的報告、慢慢傳了過來。

真的是、多麼。克萊維爾敗戰後僅僅五日。不管有多疲弊也不可能被打垮才對、這可是自己帶領的聖加侖軍。警戒網也部署著。敵人行軍、然後侵入市內、根本就不可能。
明明、是這樣的。

「到底怎麼回事！？崗哨在幹什麼──」
「現在不是詮議的場合了！將軍閣下、快逃吧！」
「──嗚、嗚庫⋯⋯」

部下的叱責、讓自己從失態恢復。確實現在說什麼也沒有用。首先必須在敵人襲擊的漩渦中脫出、從整態勢。
抱著戰傷痛苦呻吟著、鮑爾借著部下的手離開房間。

「什！？」

但是、他從房間裡出來後就再也走不了走廊上彌漫的是血的臭味。還有內臟的惡臭。從地上直到天花板全部都是、赤紅、赤紅、赤紅。也就是說敵人已經打進來了。
然後在這淒慘的光景中、理所當然般的出現了人影。

「推定是敵人司令的人物確認。確保移動」

那是全身覆蓋著盔甲、拿著大劍的戰士。平坦的聲色、從微微露出的臉來看、就像是一種讓人覺得是格雷姆錯覺般的機械、完全不像人類。那個動作也完全超過了機械以上的精密而迅速。

「快逃啊、閣──」

推走鮑爾的部下、就這樣被一刀二段、成為走廊上轉角那無數的前衛藝術的伙伴。只有一人、與敵人對峙而沒有餘裕的將軍、忍受著負傷的身體、把腰間的軍刀拔了出來。

「⋯⋯聖加侖的、不對、拜哈里耶的將領在此！」

他自認為不是森林與精兵之國聖加侖的將軍。就連劍術、以及兵卒的訓練方法也不是。放開一突、便逼近了敵人鎧甲的接縫、貫通脇下的關節部、直達心臓。

傳回來的手感、有被砍到。對於部下仇人的敵兵、口中發出了吐出血泡的聲音、就這樣即死倒在地上。尤爾根・鮑爾的劍術、沒有衰退過。但是、並不能使用多次。剛剛的一閃就讓前日的傷口裂開了、Jikujiku地感覺滲著血。

「庫、嗯⋯⋯！快、要快點脫出、與軍隊合流」

在襤褸的身體與混亂的精神中，攀援前進著。途中、被敵兵斬殺友軍的血弄髒了、也不在乎。要快點、快點與軍隊合流。死去的部下報告現在因為敵襲指揮系統奔潰了。必須收拾這個混亂場面、沒有能夠再編軍隊的人。現在對自己來說是緊要關頭、不是去管這些事情的時候了。

而在他前面出現的是、

「推定為敵司令官的人物確認。確保移動」
「注意。類似於Ｓ-５１的屍體確認。對象的威脅度向上修正」
「提言了解。最優先事項向對象確保無力化修正」

剛剛殺死的相同的敵人三體。不對。是與剛剛殺死的敵人同樣打扮的、同樣言語的三人現出現了。

（什⋯⋯這是什麼、這些傢伙！？）

同樣的鎧甲。同樣的頭盔。同樣的劍。同樣平坦的語調。與之前的士兵太相似了。簡直就像是三胞胎、不對加上最初的一人那就是四胞胎。但是、要是仔細看的話會發現身高與臉、還有聲音都是不同的人。完全相同的打扮、還有言動、不由得讓人覺得頭暈目眩。與被染上鮮血的風景融為一體、就像是在惡夢中一樣。

惡夢中的住人、化為現實出現在了鮑爾面前。

「究竟是什麼人、你們⋯⋯」

不知道誰的聲音、用著機械般的語調回答。

「我們是阿爾凱爾軍。為了確保貴官的安全」
「請理解抵抗是沒有意味的」
「還有、鎮壓中有為了不危及貴官的身心、生命的風險的義務」

然後、根據這拿著大劍的不能大意的三體敵人的話。知道已經沒有退路了。同時也沒有了抵抗力。有著不屈的意志翻山越嶺的將軍、就像是聽到了支撐自己的支柱折斷的聲音。
最後，不服輸的嘟噥著。

「這、種⋯⋯如此瞎扯的戰爭、這麼可能接受⋯⋯！」

然後他、向斷了線的人偶一樣癱倒在地上。在遙遠的意識中、那是鎧甲摩擦的聲音、以及被手觸摸的感覺。

※※※

「第四小隊報告。市街北東部的制壓、結束」
「第五、第六小隊也是同樣的結果」
「北部區域的制壓已經完成百分之八十七。要對殘敵進行掃蕩嗎？」

那是沃爾丹東部不斷進出的阿爾凱爾軍本陣。前回、克萊維爾盆地戰闘時的景象再現了、陣幕中讓人眼花繚亂的通信與報告傳來傳去。

不同的是、上次膨大量的通信管制全部由優妮一人完成、這次變成了複數的奴隷。
在那一位奴隷的請求指示下、多爾多蘭邊境伯爵稍微考慮後開口了。

「不用、市內北側鎮壓的小隊、不要掃蕩殘敵去其他方面支援。只要在後背與側面進行攻擊就可以了，這樣疲弊的敵人自然就會奔潰」
「明白了。那就這樣通告。⋯⋯Ｍ-２７向北部區域擔當的全體通達。重複、Ｍ-２７向──」

看著無機制的傳達、他深深的嘆了口氣。

與總司令官托利烏斯相談後隨行的、前回終盤後的今回、把實質的指揮權給了這邊。確實對於軍務經驗而言、多爾多蘭不是他們只用一兩就能夠跟上的。但是這個戰爭最大的當事人、不是他而是沃爾丹領主托利烏斯。政治的盟友──實質是下僕──話雖如此、這可不是能夠讓他人聽到的話。

這是為了克萊維爾遇到緊急事態──進行偽裝的必要──話雖如此、實際上讓托利烏斯自己去指揮不就行了嗎。不過話不能這麼說、

「哎呀、嘛、這種內行人的事情。就讓作為專家的邊境伯爵閣下擔任就好了。你想為家中、攢點分數也不壊吧？」

說了這樣的話。

開發出了火槍這種新兵器、下達破壊對方補給的戰略、然後開發利用土塁進行塹壕戰的戰術⋯⋯就這樣說來那個男人也已經是專家了、這樣想著。
但是、討論實際作戰的時候、也會與初歩的認識有著一定的誤解與偏差。托利烏斯為了應對這種奇妙的偏差，所以把這些東西交給了不是開發者人來戰鬥、並且進行說明。

物は言いようだな、と再び溜息。

「哎呀哎呀。怎麼樣、多爾多蘭邊境伯爵閣下？」

看到他無精打采的樣子、一位貴族過來搭話。
多爾多蘭、掩飾著苦笑起來。

「沒有、怎麼說呢。戰爭變得好快、在不知不覺間就這樣了」

口中的感慨、是敷衍了事的虛言。

現在將與兵並沒有在一起、而是通過通信禮裝進行遠隔通訊、就像是棋子一般操縱著軍隊、殺死敵人、當然自己的伙伴也會死。這種戰鬥指揮令自己背部不由得生出一股寒意。然而也只是一點點、因為現在沒有時間去品味那種心情、也不想去品味。

但是、眼前的男人並不知道多爾多蘭話的意思結果笑了。

「不不、怎麼說。這樣的戰鬥也是古今沒有的。這也是多虧了歐布尼爾伯爵閣下的手腕手段、實在是不敢當」

這樣說，莞然的笑容讓眼睛閃耀著。

這個貴族的名字是阿爾萊澤男爵。在沃爾丹州南西、那個港町便是阿爾萊澤的領地。在瑪爾蘭子爵時代便被托利烏斯洗腦、被編入自己派閥的一人。這個男人的洗腦內容與多爾多蘭是一樣的──把拒絶命令和反抗的意志削除掉。但是、為什麼這個人對托利烏斯會有這樣的好意。

「這次的戰功、會令我派迎來躍進的時候吧。新生的國軍全體都將使用我們的手法、我國的防衛也將安泰。不對、說不定連統一大陸也是有可能的哦？⋯⋯嗯、這樣說有可能過分點了。哈哈哈！」

就是這個理由嗎。

被錬金術師帶來的智慧與黃金光輝的魅力迷住的貪得無厭之人。或者是體內有著惡魔之魂的男人。就是這個阿爾萊澤男爵的正體。

雖然覺得很難看、但確實不錯。只要被托利烏斯洗腦、就沒有解除手段了。或許有但是、給自己拴上鎖的人、是不可能笑著給予自己那種慈悲的。只能徹底服從、搖著尾巴在那個男人的傘下爭取利益。對其進行指責、多爾多蘭做不到。因為大家都一樣、自己與其他的派閥成員都是一樣的。儘管如此、如此露骨的追從也實在是令人覺得不快。

多爾多蘭用厭倦了戰場一樣的心情、點了點頭。

「⋯⋯能夠有此戰功、也是靠卿提供的兵力。我為總司令官歐布尼爾向你表示感謝」
「不不、沒有這個道理。本來、這都是歐布尼爾伯爵賜與的──」
「嗯！」

對於粗心說漏了嘴的阿爾萊澤男爵、他突然變得尖銳而叱咤。

「⋯⋯這些士兵、不是你自己獨自購買的嗎？到底是怎麼回事、我らが一派には似たようなものを抱える家が多いがな」
「──哈、哈哈哈⋯⋯那、那是。不、這是因為伯爵閣下的恩德過重的緣故、把自己買的和大人賜與弄混了」

阿爾萊澤男爵臉抽筋般的浮出媚笑。

五日前克萊維爾之戰這個男人並沒有出現、因為東部奪還需要軍隊。為此屬於軍勢主力的在托利烏斯派的他──稱之為中道派的──從屬於周邊貴族的他提供援軍。這個援軍進入沃爾丹、已經是戰鬥後幾天的事情了。因此並沒有參加克萊維爾之戰、而是在這裡出兵。

然後作為中核的、便是那個男人部下配備的改造奴隷、Ｓ系列。本來二十只就能討伐西方邊境的魔物、現在有著百人的力量更是強力的戰力。

投入戰線後的效果、已經通過之前的通信管制的奴隷報告知道了。對敵人的警戒線進行少數多段階分開對敵人的城市進行突入、然後聖加侖軍就這樣被壓倒了。都市被奪回也只是時間問題。

那樣的東西要是知道是出於托利烏斯之手配備さ的話、估計會被認為對王國有叛意了、然後布洛森奴的那些傢伙的吵鬧必至。

現在、本陣正好是除了自己派閥的人以為都不在。但是、這也是阿爾萊澤男爵嘴快。要是在別的地方說漏嘴的話就麻煩了。
更何況、現在州都沃爾丹、那個近衛第二騎士團還因為休養與再編而在駐屯中。那是類似於中央集權派眼線的人。與軍隊同道、要是不小心、軍中也有可能會有人不小心說出去。

「嘛，不用那麼神經質也沒問題哦」

說著、陣幕的深處傳來打招呼的聲音、那是托利烏斯・疏日南・歐布尼爾。淒慘的焦土作戰、那個殘酷的克萊維爾迎擊戰、以及這次追擊戰的總指揮官。沃爾丹引發的大大小小的悲劇、原因都是這個男人。
他也沒有看一下部下的報告書、就這樣湊著臉過來搭話了。

「這個陣地的周圍、已經交代讓NO.系列監視了。現在可是優妮與朵萊依擔當的時間哦？這二人對野伏是很有心得的、再多的對手也無法接近」
「這樣啊」
「但是、嘛──」

然後抬起了臉、看著男爵的方向。就像是看著試藥的試管一樣、那樣冰冷的目光。

「──情報的處理要注意一點哦、這點與邊境伯爵的意見相同。特別是阿爾萊澤男爵、你的領地位置離卡納萊斯很近。奧姆利亞、卡納萊斯方面的是情報收集的老手、所以請再三小心」
「是、是的⋯⋯歐布尼爾伯爵閣下。以後、會細心注意的」

回答的聲音在顫抖，臉上不停地流著汗。沒錯，就和被蛇瞪視的青蛙一樣。

（「人喰い蛇」的樣子真是栩栩如生）

多爾多蘭內心評價著。
托利烏斯判斷覺得可以後、之後將眼光落在文件上。

「嗯、以後──我手伸不到的地方都要注意一下。這個帳篷很安全，所以可以隨便暢談。我和部下也是喜歡聊天的類型。⋯⋯好、菲姆。關於你的報告」
「是的、主人、嘟、回答。不管什麼都可以問」

錬金術師旁邊站著的是、女性形象的格雷姆、菲姆。在這個陣營中、主要是負責對新兵器進行信息功能評價。模仿人的兵器測試其他兵器。這個倒錯的狀況、令人不由得分不清真假。

「從Ｖ-01Y的性能來看、作為攻勢兵器來說Ｖ系列是沒有問題的。但是、要是從操作性和安定性來看就有問題了」
「確實、嘟、同意。與其他量產型不同、情緒領域上很難控制」
「那個性質上、要是不擅長控制吸血衝動的話、有可能會引起邏輯矛盾使精神崩潰⋯⋯⋯這樣很難『製品』的優點以及穩定感。」
「但是、從這個『製品』能夠令一軍奔潰的打擊性能也是非常有魅力的、嘟、反駁」
「不、對於攻擊力來說沒有必要拘泥於吸血鬼。讓EE系列並列、然後使用強力魔法的絨毯爆擊就行了。因為這個弱點所以這個報告基本上都是負面的。這次對手吸血鬼是備用所以才沒有問題」
「那麼、作為制壓力的方怎麼樣？嘟、提議。吸血眷族化與死靈術令頭數増加、作為怪物歩兵能夠快速増產」
「老實說、這個有點不喜歡。又不能放任不管令其增加。萬一失去控制的話就會發生大慘案了。增加過頭的不死族也是麻煩」

這是多麼、冒瀆的會話。
把人類敵人的魔物、這之中還是最上位的以危險度自豪的吸血鬼。進行使役、然後利用投入戰爭、而且現在還對那個戰果進行討論能否再利用。正常的人要是聽到了、估計會立刻前往教會要求審判托利烏斯了。

「但是、那樣的話你沒有必要這樣測試吧？」

多爾多蘭受不了插嘴了。聽到了就明白了、假裝偶然與聖加侖軍遇到吸血鬼的襲擊的、就是那年少的少女。改造完的魔物、結果就是用完扔掉。只要無價値就毀掉、不愧是不在意責備的人。
托利烏斯抬起臉。

「確實如此⋯⋯嘛、那也是實驗的一環。作為不老不死的代名詞的吸血鬼。要是能夠克服這個缺陷、僕的理想就發現近道了。但是、結果還是那樣的不禁人意啊」

仿彿毫無內疚。多爾多蘭目瞪口呆、然後他繼續說。

「就是這樣、Ｖ系列的量產只是附送的。因為是很有趣的題材所以研究了一下、所以小小的製造了一下」
「真是遺憾、嘟、發出感想。本來那個、戰鬥力是全製品中最頂尖的」
「畢竟身體能力離夏爾就僅一步之遙。雖然有點捨不得、但是也沒有辦法。不過嘛、那也不是完全沒有用處。以血液的素材或者能夠做出奇美拉化的吸血鬼⋯⋯只要把這個想法再利用的新實驗已經開始構想了。正好、這次戰鬥也出現了有趣的題材。這邊也是相當有信心的喲？也許、近期內新的『作品』又要誕生了」

話尾、好像說出了什麼不好的事情。新的『作品』到底又會誕生什麼怪物出來。或者說『作品』只是一顆棋子、用了對聖加侖軍進行以萬單位的殺戮。這次想來也會引起可怕的事態吧。

只要想像一下、就會膽怯得想逃跑。

「⋯⋯不過這也是戰爭告一段落之後的話了」
「嗯。也就是說馬上要開始了。⋯⋯還是說、要再等一段時間？」

至少也請反駁一下啊、吃人也不要這麼不露骨頭啊。真的是、吊人胃口的男人。

「不、不久就會決定了吧。只要把指揮交給我、估計就很閑了吧」
「哈哈啊、就是這樣。⋯⋯那麼、菲姆。你跟實驗室進行通信、讓先回去的塞絲準備一下。詳細內容就──」
「原來如此原來如此⋯⋯嘟、銘記。就這樣傳達指示」

由機械組裝的格雷姆行動了起來、但是沒有做出任何類似於指示的動作。那之前、一個疑問浮了起來。

「說起來主人、嘟、改變話題。伊麗莎・蘿茲蒙德・巴爾巴斯特、和麾下的近衛第二騎士團、真的就這樣不進行処分可以嗎？」
「關於那件事，我已經告訴你了。沒有必要變更。」
「那麼、嘟、確認。⋯⋯那麼、失禮了」

然後菲姆往天花板跳出。那腳步聽起來好像是不服一樣、多爾多蘭覺得是錯覺吧。
這個暫且不提、她的通信結束回來為止的時間內、關於敵司令官的人物捕捉、捕獲的消息送到了。
聖加侖軍奇襲的突然戰爭、開戰僅僅二週間就結束了。

※※※

「啊─、結束了啊⋯⋯累了、好懶、好睏⋯⋯」

數日後、追擊的州東部奪還戰結束了、我們一行人回到了州都沃爾丹。在不習慣的環境下做著不熟悉的工作神經被折磨著的我、回到了政廳設置的私室、立馬跳到了床上。

「你辛苦了、主人」

另一方面、優妮的話、則是做完各種各樣辛苦的工作後、卻還是像往常一樣沒有變化。嘛、這孩子跟因為鍛鍊和常人不一樣、這我是最了解的。
然後她、看著躺在床上的我、

「僭越了、這似乎比起平時的寢具要不好」
「這個啊。都市政廳放置的床、是不可能比貴族家之物上等的」

背後躺著的背、也覺得不舒服。儘管如此和軍隊的野營比起來還是天對地。

「那就無法解決疲勞了。⋯⋯可以的話、能夠讓我的膝蓋借你嗎？」
「呼─⋯⋯那麼、就拜託了─」

我二話不說答應了她的請求。優妮的膝枕令人非常舒心。當然、那是不可能比得上專門為睡覺而準備的枕頭柔軟的、不過她為了令人覺得舒適調整了腳的位置還有力度。體驗過的我是能夠保証的。如果真的有人在意她的話、要是給錢我也是會願意把她借出去的。

「那麼、失禮了⋯⋯請」
「嗚⋯⋯我才是失禮了⋯⋯」

在正座的她的大腿間、頭如同預想的一樣。
啊─、安心⋯⋯⋯就像是有著回到了實家般的安心感。但是、這個世界的實家實在是讓人不覺得安心。所以、就說起了那句慣用語。

「順便耳朵的掃除也拜託了─」
「知道了。那麼、耳朵」

就算是突然說出的事情、優妮也是從口袋裡掏出耳勺'。作為一個女僕應該有的一種常備品的。要是被這樣問到的話、也許是這個世界的女僕的常識吧、大概。

暫時、こりこり地耳朵被挖著、我的頭部也被固定著沉浸子那種感觸中。哈啊─、好舒服⋯⋯⋯因為這種舒適而溫暖，就這樣沉醉下去。

但是、幸福的時間是無法長久的。
門發出了被敲打的聲音。反應過來、優妮柔軟的大腿微微僵住了。

「失禮了、閣下。我是維克托爾。可以給我一點時間嗎？」

從對面傳來的、是我的家臣內政部門的雙璧之一。面對如此熱心工作的他、尷尬恢復了一點疲勞的我、必須說點什麼。

「⋯⋯你一個人嗎？沒有其他客人嗎？」
「不、就我一人⋯⋯」

太好了。不、應該說太麻煩了。

「進來吧」
「？失禮了──這是怎麼回事、你們？」

那種文件進入室內的維克托爾、最初是僵住了、接下來是露出了混合著驚訝和理解的表情。能夠短時間快速變臉。美男子這種生物、果然是先天的演員嗎。

「看不明白嗎？優妮在為我膝枕、順便。打掃一下耳朵」
「一看就明白了⋯⋯⋯你自己、居然沒有自覺到墮落嗎？」
「？」
「請不要露出那麼不可思議的臉！這個世界、有哪個是勝戰回來後最先做的是、讓女性進行膝枕的貴族！？」

咔啊─、好像還發出了類似的擬音呲和捲起海浪的背景。難道有那麼稀奇嗎？貴族也是生物、喜歡讓女人服侍也不奇怪吧。

「女僕長也是。我都再三說過不要嬌慣閣下了吧！？」
「不好意思這個提案難以接受、維克托爾卿。主人現在非常疲勞。那麼稍微緩解一下、也是作為從者的義務」

她這樣說著、然後手又移到了我的肩膀的位置。說話間、那個手指就像是在彈鋼琴演奏一樣、按摩著我的肩膀。
真是勤快的奴隷。那麼我也稍微補刀、進行援護射擊吧。

「沒錯沒錯。就是這樣、這是對努力的我、給優妮和我自己的獎勵」
「說的跟肺腑之言一樣。真是沒節操。要是這個被看到，服侍的人都會失去忠誠吧」
「怎麼這樣說、維克托爾。⋯⋯服侍我的人、不是都已經被改造了腦無法背叛了嗎」

就如同我說的一樣、被改造過的人到現在為止一個都沒有背叛過。是的、直接駁倒。

「哈啊⋯⋯明白了、可以了。那麼、維持那個姿勢也沒關係、請聽報告」
「是的是的─。⋯⋯做到了、優妮。這是我們的勝利」
「恭喜了、主人」
「這是哪來的勝負啊、哪來的⋯⋯」

維克托爾太陽穴抽動著、翻開了書類文件的第一頁。

「那麼，首先簡單地說一下我方的損害。這是記錄了詳細數字整理過的文件，回頭請確認」
「嗯。知道了。那麼、怎麼樣？」
「總的來說。殘酷的想哭。州東部本期收獲全滅。因為聖加侖軍的略奪全部沒有了、更因為被一把火燒了田地也全部毀了。哎呀哎呀、真是過分」
「嗚哇啊、聖加侖軍真是過分啊」
「是的。在主人的領地做出這種事情、真是無法原諒⋯⋯」

我們附和著、他也裝傻似的看著這裡。這是什麼表情、美男子的臉可是糟蹋了。
但是、嘛、這也不是沒辦法嗎。如果不進行焦土作戰燒掉的話、就會被敵軍奪去。不這樣的話、我們的勝利要是遅了、那麼就會被趕過來的増援奪去成功、然後被趕出沃爾丹。以其為這種損害嘆息、還不如為勝利喝彩。

「⋯⋯要繼續咯？土地的損失也很嚴重、人力的損失則是在這以上。總之、初戰中士兵就失去了二千人、克萊維爾之戰徵兵的民眾則是犧牲了一萬人以上。這是什麼、這種損失情況？位數的差異也太過了吧」

如同維克托爾所說的一樣、會戰一次就出現以萬為單位的犧牲者、一般是不可能的。這個世界、損失擴大到如此的場合基本都是退兵。或者說、遇到如此激戰士兵早就逃跑了。因為對於原民兵來說、貴族之間的戰爭、根本不值得自己把命送出去。

但是、這次的情況非常特殊。

「因為進入了無法逃跑的塹壕、又在Ｍ系列與Ｂ系列的督戰下。這種情況下想要逃離戰爭、絶對是不可能的」

是的、那個因為野戰工事建立起來的壕。不僅僅是用於防御、也有為了不讓士兵逃跑的目的。如果不是像是被塞入墓穴一樣、同時在我的監視下。那些拿著槍沒有訓練的兵隊、早就逃散了。

這就跟牧場的柵欄一樣。能夠防止恐怖的狼守護家畜、同時也不能容許從屠殺中逃跑。因為逃出來、只會變成死人。
維克托爾嘆了口氣。說起來、我的周圍的人嘆息的次數不會太大了吧？

「真是的、如此粗暴的用人方法。而且死去的大家、都不是傭兵而是人民徵募的士兵。這些男人一下子全部消失了、回給今後的領地經營帶來麻煩的哦？而且不僅僅是戰闘、面對敵軍的略奪行為與暴行、非戰闘員的也有損失。以考慮到這些金額、就不由得頭痛」

雖然所有貴族都是輕視平民的、但是平民是作為稅收的來源所以也要避免最大的損失。平民就是會生出金蛋的家畜、重視這些的才算是一個合格的貴族。

「關於那裡⋯⋯嘛、就用你和魯貝爾考慮的方案吧。中央集權派的態度如何」
「消耗的『製品』補填也是必要的。現在對於EE系列來說、多少是可以、但是生產成本太高了」

優妮這麼說。再次戰鬥、Ｍ系列與Ｂ系列也有參戰、有一些也死在了敵手上損耗了不少。為了填補這個空缺、如同她所說EE系列的成本過高、而且精靈種在人類社會運用也很難。看來初期型的『製品』們也要加油啊。

借給阿爾萊澤男爵的Ｓ系列、則是基本無傷非常順利。除了幾隻因為別的原因損耗了。不愧是修羅之國聖加侖連邦王國、居然進行二線級的奇襲也會出現了損耗。

「人員與物資與資金都被大量消費了、最後的結局是領地被大量荒廢。真是的、戰爭就是這麼討厭」
「是的。這也是因為聖加侖沒事打過來。還有、這個國家的那個誰在煽動」

真是的、好像都看到了那個腐爛的老頭殘酷的眼神了。但是、這些支出也獲得了回報。總之、這邊可是為正在害怕的國王陛下保護土地而奮闘著。就算什麼挖苦的話、也不會說到我這裡來。

「不過、這場戰爭終於結束了。不對、沒有進行和平交渉的談判、我們還沒站住腳。如果說要繼續戰爭、王都估計會募集士兵與我們合流。總之我們要做好充分準備」
「好的。現在四萬人的敵軍、已經從地上消滅了。看一下現在的局勢、現在王都被立下了無能的戰果另一邊則是一只慾望無窮的餓狼、就是這樣吧。以後要是出現向他國懲罰而出兵的藉口、估計會選擇沃爾丹這個一度被敵國攻擊的土地。但是我們要守衛這個領地」

維克托爾露出了一臉壊人的笑容。

我們初戰十分成功。要是想繼續戰鬥的話、估計不會來找我們。其他貴族也是想要建立武功而獲得恩賞、因此也就避免了我要再次獲得戰果的事態。第一、防御戰是成功了但是面對的是這種損害。但是如果在任意驅使的話、就會有為了改善待遇而罷工的可能性。

「也就是說、有著一大堆事情要処理。首先是接見行軍中王都的援軍的使者。給予大軍一段時間的食物。還有要給予聖加侖軍俘虜的物資。這裡明顯是沒有那種餘裕」
「確實。自己都不一定有吃的、就更加不可能給客人了」
「加之這邊是良好場所但是戰力稀薄。而且有可能會有保有兵力的敵軍來投降。然後還有各種各樣的問題」

然後三人同時發出了嗚─嗯的呻吟。總之現在的沃爾丹就是一個大坑。食物缺少甚至不能養戰俘、兵力也沒有好不容易抓住的敵兵想要拘留也很難。而且侵略之時略奪後還放了火、又攻擊主持停戰的指揮官、沃爾丹領民現在反聖加侖感情已經是處於最惡的狀態。嘛、雖然一半分以上都是我煽動的。暫且來說、這樣的土地上要是長時間放著捕獲的敵人、估計沒好事。要早一點移到別的地方去。

「那麼、使者就交給第二騎士團吧。然後我派的貴族也派出隨員。把戰俘引渡、就在諾維諾附近交涉」

維克托爾說的地名、是位於沃爾丹州西北。王國南方與東方的交通要衝、作為大軍駐留的都市。如果沃爾丹陥落的話、這個地就會變成兵家奪還之地。合流時作為戰俘引渡也非常合適的位置。

「那麼、就在那個地方吧」
「明白了。但是說起第二騎士團我想起來了」

說著、優妮的大腿估計有僵硬了。同時維克托爾沒有注意到。當然作為主內政的青年貴族是不可能注意到的。

「讓那傢伙能活下去可以嗎？說實話、她讓人感覺完全無法與閣下相容」

近衛第二騎士團。那個團長伊麗莎・蘿茲蒙德・巴爾巴斯特。她們的、不、她的危險性是能夠理解的。與Ｖ-01Y的戰闘観戰時優妮也忠告過、其他的NO.系列意見也相同。不、除了某個說著「要是這樣處分的話作為美人還有處女就太可惜了」云云戯言的。還有、杜耶也判斷為保留。

坦白說、我也想盡快處理。畢竟是那個老頭的武力集團之長、還是喜歡死站的戰闘狂與野蠻人。與我的相性基本就是水與油、讓她活著只會留下危險的禍根。

但是。

「不要在這樣說了。魯貝爾也是、你的同意也是一樣的吧？」

那倔強的大姐姐、就是政治的爆彈。是不能隨便出手的人。

「就是這樣。能夠理解就好。雖然形式上是切斷了關係、但是那也是地方分權派的大物巴爾巴斯特侯爵的女兒、輕易処分太危險了」

沒錯、這是令人無法忘記的、麻煩的大貴族千金。與家庭縁切斷關係變成騎士成為別家、然後完全無關、是不可能的。實家也想讓她隨便在外面浪一有機會就會寫信、然後送些讓她覺得不自由的新禮服。當然本人估計也注意到了、恢復舊好的意思可見一斑。要是認為可以隨便找那個女兒的麻煩、是非常天真的事情。

「戰爭結束了、今後將會是正式和那個老頭決一勝負的嗜好了、這時候要是有新的政敵登場會很困擾的。總不能連地方分權派也得罪吧、他是也是有著良知的穩健派。這次估計能夠成為交渉的窓口吧、胸襟を緩める程度の好意があればそれ以外の手立ても取りやすくなられるかと」
「如果那能夠像多爾多蘭邊境伯爵那樣洗腦、接受我的招待就好了」

也不能排除這次事件令我被認為是女兒死去的原因、最後誘導著變成棋子、態度硬化成為敵人的可能性。伊麗莎的戰闘力確實是不安要素、但與現段階這個不安比起來。地方分權派的危險比起來優先度較低。就是這樣。

「主人與維克托爾卿的判斷、我知道是正確的」

說著、優妮的身體稍微軟化了。

「優妮覺得那個姐姐恐怖嗎？」
「說實話、就是如此。以數値化、明文化來說很難、有種來歷不明的感覺」

在這孩子的決定事項裡這是很罕見的。那是感性的差異吧。是我與維克托爾達不一樣的思考、優妮是處於戰鬥的角度。屬於戰闘者的她、明明不是NO.系列卻打倒了Ｖ-01Y。伊麗莎小姐的戰鬥、感銘的同時也感覺到了危險。

但是、請放心。坦白的說、我是天生的膽小鬼。被我認定的敵人十有八九、是不可能讓其活著的吧？

「嘛、只要那個老頭一死就可以了。這樣就結束了、國內的不安要素也解決了」

我做出與聖加侖兩敗俱傷的拉瓦萊侯爵的計劃、已經破產了。現在便是我的回合了。不僅趁勢奪走侯爵的物資與財貨、在戰爭時期的時候還偶然奪回了權力、這場戰爭已經結束了。至於這次戰災的損害、那個責任完全可以通過政治闘爭取回。

老頭打算平安無事的、在自己主導下驅逐敵國軍隊、建立武功的計劃破產了。現在、那個成果被我奪取了。那個老頭、現在只能負起導致聖加侖侵略的責任了、不僅沒有了信用還有著令國家與敵國進行簽署不平等條約的前科。雖然戰爭中獲勝使國家得到利益、也不會這麼簡單就結束。就如同之前所說、那些人不會允許我們單獨奪取功績。而追究的風暴中、那個瘦弱的古狐就會成為彈劾的中心。

對於罕見地有著好戰的心情笑著的我、維克托爾也是一樣同意了。

「那個令人忌諱的老頭、估計會被粛清吧。暗闘中估計還要一些有著武力的棋子」
「但是、現在不是時候。高舉實力主義的第二騎士團不在王都」

對於只是裝飾的第一騎士團、還有地方分權派的過激派眼裡。弄清誰才是在沃爾丹這次戰鬥的最有力嫌疑犯是沒有必要的。重複說明著、優妮對她也慢慢妥協了、

「如果只是這樣的話⋯⋯」

最後勉勉強強結束了。呀哩呀哩、和這孩子意見不一致、這實際上是是第一次嗎？但是、之後優妮都會拘泥著記住這一點吧。伊麗莎・蘿茲蒙德・巴爾巴斯特、是那個拉瓦萊之外的第一抹殺候補。應該是這樣考慮的。

「那麼，關於怎樣對付近衛就這樣了。下面的報告──」

還要繼續啊。我都覺得麻煩了。真是的、事情結束後、往往都是戰爭的麻煩最多。有不能敷衍了事。
嘛、雖然不久後麻煩都會消失就是了。總之距離最近的戰爭已經是五十年前了。那麼下次戰爭的到來也要等很久了。距離下次戰爭的準備、還有著相當長的時間。

之後就是國內問題──那個老頭、還有作為樞紐的近衛的問題。之後就能慢慢研究了。
我的頭部享受著優妮的大腿、帶著如意算盤。

緊接之後，那個計劃一下子就破產了。
根據魯貝爾的諜報機關帶回來的情報。

得到的消息是、中央集權派的首魁拉瓦萊侯爵、病發陷入了危機。